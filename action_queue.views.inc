<?php

/**
 * @file
 * Provide views data for action_queue.module.
 */

/**
 * Implements hook_views_data().
 */
function action_queue_views_data() {
  $entity_type_manager = \Drupal::entityTypeManager();
  // @todo Add support for all content entities.
  $supported_entities = [
    'node',
    'user',
  ];
  foreach ($supported_entities as $entity_type_id) {
    $entity_type = $entity_type_manager->getDefinition($entity_type_id);
    $base_table = $entity_type->getBaseTable();
    $key = $entity_type->getKey('id');
    $data[$base_table][$key]['relationship'] = [
      'title' => t('Action Queue'),
      'label' => t('Action Queue'),
      'help' => t('Action queue items referencing this %entity_type.', ['%entity_type' => $entity_type->getLabel()]),
      'id' => 'standard',
      'base' => 'action_queue_data',
      'base field' => 'entity_id',
    ];

    // Add the reverse relationships on action queue items.
    $pseudo_field_name = 'reverse_target_id_' . $key;
    $data['action_queue_data'][$pseudo_field_name]['relationship'] = [
      'title' => $entity_type->getLabel(),
      'label' => $entity_type->getLabel(),
      'help' => t('The %entity_type is associated with this action.', ['%entity_type' => $entity_type->getLabel()]),
      'id' => 'standard',
      'base' => $entity_type->getDataTable() ?: $entity_type->getBaseTable(),
      'base field' => $key,
      'group' => $entity_type->getLabel(),
      'entity_type' => $entity_type_id,
      'field' => 'entity_id',
    ];
  }

  $data['action_queue_data']['action_item_bulk_form'] = [
    'title' => t('Action item operations bulk form'),
    'help' => t('Add a form element that lets you run operations on multiple action items.'),
    'field' => [
      'id' => 'action_item_bulk_form',
    ],
  ];

  return $data;
}

/**
 * Implements hook_views_data_alter()
 */
function action_queue_views_data_alter(array &$data) {
  // Changing the action plugin filter to a select list.
  $data['action_queue_data']['action']['filter'] = [
    'id' => 'action_queue_action_filter',
    'allow empty' => TRUE,
  ];
  $data['action_queue_data']['action']['field'] = [
    'id' => 'action_label',
  ];
}
