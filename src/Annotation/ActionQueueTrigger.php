<?php

namespace Drupal\action_queue\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an action queue trigger annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class ActionQueueTrigger extends Plugin {

  /**
   * The action queue trigger plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The admin label of the action queue trigger plugin.
   *
   * @var string
   */
  public $admin_label;

}
