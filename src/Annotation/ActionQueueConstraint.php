<?php

namespace Drupal\action_queue\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an action constraint annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class ActionQueueConstraint extends Plugin {

  /**
   * The action constraint plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the action constraint plugin.
   *
   * @var string|\Drupal\Core\Annotation\Translation
   */
  public $label;

}
