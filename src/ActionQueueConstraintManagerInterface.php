<?php

namespace Drupal\action_queue;

use Drupal\Core\Action\ActionInterface;

/**
 * Provides an Action constraint plugin manager.
 */
interface ActionQueueConstraintManagerInterface {

  /**
   * Get the constraints by action plugin.
   *
   * @param \Drupal\Core\Action\ActionInterface $action
   *   The action plugin.
   *
   * @return array
   *   The array of applicable constraints.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getConstraintsByAction(ActionInterface $action);

}
