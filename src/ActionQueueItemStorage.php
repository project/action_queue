<?php

namespace Drupal\action_queue;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Storage handler for the action queue item entity.
 *
 * @see \Drupal\action_queue\Entity\ActionQueueItem
 */
class ActionQueueItemStorage extends SqlContentEntityStorage {

}
