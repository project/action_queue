<?php

namespace Drupal\action_queue;

use Drupal\Core\Entity\EntityInterface;
use Drupal\action_queue\Entity\ActionQueueItemInterface;

/**
 * Provides an Action Queue Execution context.
 *
 * This is slightly different from the ActionExecutionContextInterface in that
 * it adds support for action queue items.
 *
 * @see plugin_api
 */
class ActionQueueExecutionContext extends ActionExecutionContextBase {

  /**
   * @param \Drupal\action_queue\Entity\ActionQueueItemInterface $item
   *   The action queue item entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param $key
   *   The optional context key.
   */
  public function __construct($action, ActionQueueItemInterface $item, EntityInterface $entity, $key = NULL) {
    parent::__construct($action, $entity, $key);
    $this->item = $item;
  }

  /**
   * The action queue item.
   *
   * @var \Drupal\action_queue\Entity\ActionQueueItemInterface
   */
  protected $item;

  /**
   * {@inheritdoc}
   */
  public function setItem(ActionQueueItemInterface $item) {
    $this->item = $item;
  }

  /**
   * {@inheritdoc}
   */
  public function getItem() {
    return $this->item;
  }

}
