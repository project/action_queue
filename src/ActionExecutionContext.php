<?php

namespace Drupal\action_queue;

/**
 * Provides an Action Queue Execution context.
 *
 * This is slightly different from the ActionQueueExecutionContext in that
 * it doesn't require an action queue item. This is useful if you want to use
 * the constraint validator in different contexts.
 *
 * For example, you would initialize the validator with this context instead
 * of ActionQueueExecutionContext.
 *
 * @see plugin_api
 */
class ActionExecutionContext extends ActionExecutionContextBase {

}
