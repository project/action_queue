<?php

namespace Drupal\action_queue;

use Drupal\action_queue\Entity\ActionQueueItemInterface;
use Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface;
use Drupal\action_queue\Event\ActionQueueConstraintsValidateEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Provides an Action constraint validator service.
 *
 * @see plugin_api
 */
class ActionQueueConstraintValidator implements ActionQueueConstraintValidatorInterface {

  /**
   * An event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The action execution context.
   *
   * @var ActionExecutionContextInterface
   */
  protected $context;

  /**
   * Store initialized contexts.
   *
   * @var array
   */
  protected $initializedContexts = [];

  /**
   * Constructor for Action Constraint Validator.
   *
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An event dispatcher instance.
   */
  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function initialize(ActionExecutionContextInterface $context) {
    $cacheKey = $context->generateCacheKey();
    if (!isset($this->initializedContexts[$cacheKey])) {
      $this->initializedContexts[$cacheKey] = $context;
    }

    $this->context = $this->initializedContexts[$cacheKey];
  }

  /**
   * {@inheritdoc}
   */
  public function validate(ActionQueueConstraintInterface $constraint) {
    $constraint->setEntity($this->context->getEntity());
    $constraint->setContext($this->context);

    if ($constraint->validate() === FALSE) {
      $id = $constraint->getPluginId();
      $this->context->addViolation($id);

      $this->context->addMessage($id, $constraint->getMessage());

      $metadata = $constraint->getMetadata();
      if (!empty($metadata)) {
        $this->context->addMetadata($id, $metadata);
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function validateConstraints($constraints) {
    foreach ($constraints as $constraint) {

      if ($this->context->isConstraintValidated($constraint->getPluginId())) {
        continue;
      }

      $this->context->markConstraintAsValidated($constraint->getPluginId());

      $this->validate($constraint);
    }

    // Dispatch the validate constraints event.
    $event = new ActionQueueConstraintsValidateEvent($this->isValid(), $this->context);
    $this->eventDispatcher->dispatch($event, ActionQueueConstraintsValidateEvent::NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function isValid() {
    if (!empty($this->context->getViolations())) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return $this->context;
  }

}
