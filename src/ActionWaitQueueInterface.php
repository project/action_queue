<?php

namespace Drupal\action_queue;

use Drupal\Core\Action\ActionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\action_queue\Plugin\ActionQueueActionInterface;

/**
 * Action wait queue service.
 *
 * @package Drupal\action_queue
 */
interface ActionWaitQueueInterface {

  /**
   * Determine whether this action can be queued or not.
   *
   * @param \Drupal\Core\Action\ActionInterface $plugin
   *   The action to be queued.
   *
   * @return bool
   *   TRUE if supported, FALSE otherwise.
   */
  public function isSupportedAction(ActionInterface $plugin);

  /**
   * Create a new Action ready to be queued.
   *
   * This is a convenient way to create a properly formatted action plugin with
   * all proper values and trigger arguments.
   *
   * @param string $action
   *   The action plugin id.
   * @param array $values
   *   The values passed to the action.
   * @param array $trigger_arguments
   *   The arguments passed to the trigger.
   *
   * @return \Drupal\action_queue\Plugin\ActionQueueActionInterface
   *   The action plugin
   */
  public function prepareAction($action, array $values = [], array $trigger_arguments = []);

  /**
   * Add item to event queue.
   *
   * @param \Drupal\action_queue\Plugin\ActionQueueActionInterface $plugin
   *   The plugin instance.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addItem(ActionQueueActionInterface $plugin, EntityInterface $entity);

  /**
   * Remove any items added to the queue.
   *
   * @param \Drupal\action_queue\Plugin\ActionQueueActionInterface $plugin
   *   The plugin instance.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function removeItem(ActionQueueActionInterface $plugin, EntityInterface $entity);

  /**
   * Helper function for loading an existing items in the queue.
   *
   * @param \Drupal\action_queue\Plugin\ActionQueueActionInterface $plugin
   *   The action plugin.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function loadItem(ActionQueueActionInterface $plugin, EntityInterface $entity);

  /**
   * Remove any executed actions.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function clearExecutedItems();

  /**
   * Fetch the action items by a certain entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The target entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The array of action queue items.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getItemsByEntity(EntityInterface $entity);

  /**
   * Trigger the actions.
   *
   * @param string $trigger
   *   The trigger plugin name.
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The entities to execute on. If left empty it will execute on all the
   *   entities found in the action queue with this trigger.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function triggerActions($trigger, array $entities = []);

}
