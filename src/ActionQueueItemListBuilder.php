<?php

namespace Drupal\action_queue;

use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Builds a listing of action queue item entities.
 *
 * @ingroup action_queue
 */
class ActionQueueItemListBuilder extends EntityListBuilder {

  /**
   * The query.
   *
   * This is used with getEntityIds() in order to control which results we want
   * to show.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $query;

  /**
   * Get the entity query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query interface.
   */
  public function getQuery() {
    if (empty($this->query)) {
      $this->query = $this->getStorage()->getQuery();
    }
    return $this->query;
  }

  /**
   * Builds a row for an entity in the entity listing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for this row of the list.
   *
   * @return array
   *   A render array structure of fields for this entity.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::render()
   */
  public function buildRow(EntityInterface $entity) {
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($entity->get('uid')->target_id);
    // @todo dependency injection.
    $dateFormatter = \Drupal::service('date.formatter');

    $row['id'] = $entity->id();
    $row['title'] = $entity->label();
    $row['message'] = $this->getMessage($entity);
    $row['created'] = $dateFormatter->format($entity->get('created')->value, 'custom', 'D, Y-m-d - h:i:s');
    $row['user'] = $user->toLink();
    $row['status'] = $this->getStatus($entity);
    $row['operations']['data'] = $this->buildOperations($entity);
    return $row;
  }

  /**
   * Get the message.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The message.
   */
  public function getMessage(EntityInterface $entity) {
    if (!empty($entity->getMessage())) {
      return $entity->getMessage();
    }

    // If no message is set then fallback to the value.
    $value = $entity->getValue();
    if (!empty($value)) {
      return reset($value);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->hasLinkTemplate('details-form')) {
      $details_url = $entity->toUrl('details-form');

      $operations['details'] = [
        'title' => $this->t('Details'),
        'weight' => 15,
        'url' => $this->ensureDestination($details_url),
      ];
    }

    return $operations;
  }

  /**
   * Return the status of the action queue item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The current status.
   */
  public function getStatus(EntityInterface $entity) {
    // If Action States is enabled and this has a value then show that.
    if (!empty($entity->get('action_state')->value)) {
      return $entity->get('action_state')->value;
    }

    // Otherwise show active or inactive.
    if ($entity->get('status')->value) {
      return $this->t('Active');
    }

    return $this->t('Inactive');

  }

  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   *
   * @see \Drupal\Core\Entity\EntityListBuilder::render()
   */
  public function buildHeader() {
    $row['id'] = $this->t('ID');
    $row['name'] = $this->t('Name');
    $row['message'] = $this->t('Message');
    $row['created'] = $this->t('Created');
    $row['user'] = $this->t('User');
    $row['status'] = $this->t('Status');
    $row['operations'] = $this->t('Operations');
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $this->getQuery()->accessCheck(TRUE)->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $this->getQuery()->pager($this->limit);
    }
    return $this->getQuery()->execute();
  }

  /**
   * Render the table of action queue items.
   *
   * @return array
   *   The table of action queue items.
   */
  public function render() {
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->getTitle(),
      '#rows' => [],
      '#empty' => $this->t('There are no @label currently queued.', ['@label' => $this->entityType->getPluralLabel()]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    foreach ($this->load() as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['table']['#rows'][$entity->id()] = $row;
      }
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    return $build;
  }

  /**
   * Gets the title of the page.
   *
   * @return string
   *   The title.
   */
  protected function getTitle() {}

}
