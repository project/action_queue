<?php

namespace Drupal\action_queue;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Action\ActionInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Action constraint plugin manager.
 *
 * @see plugin_api
 */
class ActionQueueConstraintManager extends DefaultPluginManager implements ActionQueueConstraintManagerInterface {

  /**
   * Constructs a new class instance.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ActionQueue/Constraint', $namespaces, $module_handler, 'Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface', 'Drupal\action_queue\Annotation\ActionQueueConstraint');
    $this->alterInfo('action_queue_constraint');
    $this->setCacheBackend($cache_backend, 'action_queue_constraint');
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraintsByAction(ActionInterface $action) {
    $definition = $action->getPluginDefinition();
    $constraints = [];
    if (isset($definition['action_queue']['constraints'])) {
      $constraint_ids = $definition['action_queue']['constraints'];
      $constraints = [];
      foreach ($constraint_ids as $id => $options) {
        $constraints[] =  $this->create($id, $options);
      }
    }
    return $constraints;
  }

  /**
   * Create a new constraint plugin.
   *
   * @param string $name
   *   The name of the constraint.
   * @param mixed $options
   *   The constraint options.
   *
   * @return \Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface
   *   The action constraint plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function create($name, $options) {
    if (!is_array($options)) {
      // Plugins need an array as configuration, so make sure we have one.
      // The constraint classes support passing the options as part of the
      // 'value' key also.
      $options = isset($options) ? ['value' => $options] : [];
    }
    return $this->createInstance($name, $options);
  }

}
