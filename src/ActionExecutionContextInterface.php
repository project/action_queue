<?php

namespace Drupal\action_queue;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface for Action execution contexts.
 *
 * This is used during action constraint validation in order to consolidate
 * the results of validation.
 *
 * @see plugin_api
 */
interface ActionExecutionContextInterface {

  /**
   * Generates the cache key.
   *
   * This is used along with the validator in order to make sure we
   * don't re-evaluate constraints.
   *
   * @return string
   *   The validator cache key.
   */
  public function generateCacheKey();

  /**
   * Get the action plugin name.
   *
   * This is primarily used with generateCacheKey().
   *
   * @return string
   */
  public function getAction();

  /**
   * Gets the current context key.
   *
   * Contexts keys are a useful way for distinguishing contexts from each other.
   * For example, it can be used to distinguish execution from validation.
   *
   * @return string
   *   The current context key.
   */
  public function getKey();

  /**
   * Set the current entity being evaluated.
   *
   * @param EntityInterface $entity
   *   The current entity.
   */
  public function setEntity(EntityInterface $entity);

  /**
   * Get the current entity being evaluated.
   *
   * @return EntityInterface
   *   The current entity.
   */
  public function getEntity();

  /**
   * Save validation metadata.
   *
   * @param string $constraint
   *   The constraint id.
   * @param array $data
   *   The data.
   */
  public function addMetadata($constraint, $data);

  /**
   * Set the validation metadata.
   *
   * @param array $data
   *   The array of validation metadata. Keyed by constraint.
   */
  public function setMetadata(array $data);

  /**
   * Get validation metadata.
   *
   * This is useful for attaching additional relevant data during validation.
   * This could be used for followup logic, display additional details etc.
   *
   * @return array
   *   An array of validation metadata.
   */
  public function getMetadata();

  /**
   * Save validation violation.
   *
   * @param string $constraint
   *   The constraint id.
   */
  public function addViolation($constraint);

  /**
   * Set validation violations.
   *
   * @param $violations
   *   The array of violations
   */
  public function setViolations($violations);

  /**
   * Get validation violations.
   *
   * @return array
   *   An array of violations.
   */
  public function getViolations();

  /**
   * Save validation message.
   *
   * @param string $constraint
   *   The constraint plugin id.
   * @param string $message
   *   The message id.
   */
  public function addMessage($constraint, $message);

  /**
   * Set the validation messages.
   *
   * @param array $messages
   *   The array of validation messages, keyed by constraint.
   *
   * @return mixed
   */
  public function setMessages(array $messages);

  /**
   * Get validation messages.
   *
   * @return array
   *   An array of messages.
   */
  public function getMessages();

  /**
   * Mark this constraint as validated.
   *
   * @param string $constraint
   *   The constraint plugin id.
   */
  public function markConstraintAsValidated($constraint);

  /**
   * Determine whether this constraint has been validated.
   *
   * @param string $constraint
   *   The constraint plugin id.
   */
  public function isConstraintValidated($constraint);

}
