<?php

namespace Drupal\action_queue\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Upcoming Actions.
 */
class ActionsQueuedListingControllerBase extends ControllerBase {

  /**
   * The current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new EntityActionsQueuedListingController instance.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route_match service.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
    );
  }

  /**
   * Get the current context.
   *
   * @return array
   *   The context.
   */
  protected function getContext() {
    return [];
  }

  /**
   * Returns list of queued upcoming actions and link to add more.
   *
   * @return array
   *   The render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function build() {
    // @todo do we need to show executed items?
    $context = $this->getContext();
    $list_builder = $this->entityTypeManager()->getListBuilder('action_queue_item');
    $list_builder->getQuery()->condition('executed', 0);

    if (!empty($context)) {
      $entity = $context['entity'];
      $list_builder->getQuery()
        ->condition('entity_type', $entity->getEntityTypeId())
        ->condition('entity_id', $entity->id());
    }

    return $list_builder->render();
  }

}
