<?php

namespace Drupal\action_queue\Event;

use Drupal\Core\Entity\EntityInterface;
use Drupal\action_queue\Plugin\ActionQueueActionInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event for when an action to add to the queue.
 */
class AddActionToQueueEvent extends Event {

  /**
   * The action plugin.
   *
   * @var \Drupal\action_queue\Plugin\ActionQueueActionInterface
   */
  protected $action;

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Builds a new add action to queue event.
   *
   * @param \Drupal\action_queue\Plugin\ActionQueueActionInterface $action
   *   The action plugin.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being sent with this action.
   */
  public function __construct(ActionQueueActionInterface $action, EntityInterface $entity) {
    $this->action = $action;
    $this->entity = $entity;
  }

  /**
   * Returns the action associated with this event.
   *
   * @return \Drupal\action_queue\Plugin\ActionQueueActionInterface
   *   The action.
   */
  public function getAction() {
    return $this->action;
  }

  /**
   * Returns the entity associated with the Event.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity() {
    return $this->entity;
  }

}
