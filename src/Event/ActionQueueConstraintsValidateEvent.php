<?php

namespace Drupal\action_queue\Event;

use Drupal\action_queue\ActionExecutionContextInterface;
use Drupal\Component\EventDispatcher\Event;

/**
 * Event for when an action to add to the queue.
 */
class ActionQueueConstraintsValidateEvent extends Event {

  /**
   * Action constraints validate event.
   *
   * @Event
   *
   * @var string
   */
  const NAME = 'action_constraints.validate';

  /**
   * Flag if the constraint validaton passed.
   *
   * @var bool
   */
  protected $valid;

  /**
   * The current context.
   *
   * @var \Drupal\action_queue\ActionExecutionContextInterface
   */
  protected $context;

  /**
   * @param \Drupal\action_queue\ActionExecutionContextInterface $context
   *   The action queue execution context.
   */
  public function __construct($valid, ActionExecutionContextInterface $context) {
    $this->valid = $valid;
    $this->context = $context;
  }

  /**
   * Return whether constraints were valid or not.
   *
   * @return bool
   *   TRUE if all constraints passed, FALSE otherwise.
   */
  public function isValid() {
    return $this->valid;
  }

  /**
   * Get the action queue execution context.
   *
   * @return \Drupal\action_queue\ActionExecutionContextInterface
   */
  public function getContext() {
    return $this->context;
  }

}
