<?php

namespace Drupal\action_queue\Event;

/**
 * Contains all events thrown in the Flag module.
 */
final class ActionQueueEvents {

  /**
   * Action added to the queue.
   *
   * @Event
   *
   * @var string
   */
  const ACTION_ADDED = 'action_queue.action_added';

  /**
   * Action added removed from the queue.
   *
   * @Event
   *
   * @var string
   */
  const ACTION_REMOVED = 'action_queue.action_removed';

}
