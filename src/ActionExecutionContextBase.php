<?php

namespace Drupal\action_queue;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an Action Queue Execution context base class.
 *
 * @see plugin_api
 */
abstract class ActionExecutionContextBase implements ActionExecutionContextInterface {

  /**
   * The action plugin name.
   *
   * @var string
   */
  protected $action;

  /**
   * Context key.
   *
   * @var string
   */
  protected $key;

  /**
   * The entity being evaluated.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Messages created during validation.
   *
   * @var array
   */
  protected $messages = [];

  /**
   * The array of violations.
   *
   * @var array
   */
  protected $violations = [];

  /**
   * Violation metadata.
   *
   * @var array
   */
  protected $metadata = [];

  /**
   * Stores which class constraints have been validated.
   *
   * @var array
   */
  protected $validatedConstraints = [];

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param $key
   *   The optional context key.
   */
  public function __construct($action, EntityInterface $entity, $key = NULL) {
    $this->action = $action;
    $this->entity = $entity;
    $this->key = $key;
  }

  /**
   * {@inheritdoc}
   */
  public function generateCacheKey() {
    return 'action:' . $this->action . ':' . $this->entity->id();
  }

  /**
   * {@inheritdoc}
   */
  public function getAction() {
    return $this->action;
  }

  /**
   * {@inheritdoc}
   */
  public function getKey() {
    return $this->key;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity($entity) {
    $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function addMetadata($constraint, $data) {
    $this->metadata[$constraint] = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setMetadata($data) {
    $this->metadata = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * {@inheritdoc}
   */
  public function addViolation($constraint) {
    $this->violations[] = $constraint;
  }

  /**
   * {@inheritdoc}
   */
  public function setViolations($violations) {
    $this->violations = $violations;
  }

  /**
   * {@inheritdoc}
   */
  public function getViolations() {
    return $this->violations;
  }

  /**
   * {@inheritdoc}
   */
  public function addMessage($constraint, $message) {
    $this->messages[$constraint] = $message;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessages($messages) {
    return $this->messages = $messages;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessages() {
    return $this->messages;
  }

  /**
   * {@inheritdoc}
   */
  public function markConstraintAsValidated($constraint) {
    $this->validatedConstraints[$constraint] = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isConstraintValidated($constraint) {
    return isset($this->validatedConstraints[$constraint]);
  }

}
