<?php

namespace Drupal\action_queue\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\StatusItem;

/**
 * Defines a content entity for storing action queue plugin data.
 *
 * @ingroup action_queue
 *
 * @ContentEntityType(
 *   id = "action_queue_item",
 *   label = @Translation("Action Queue Item"),
 *   label_collection = @Translation("Action Queue Items"),
 *   label_singular = @Translation("action queue item"),
 *   label_plural = @Translation("action queue items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count action queue item",
 *     plural = "@count action queue items",
 *   ),
 *   base_table = "action_queue_data",
 *   handlers = {
 *     "storage" = "Drupal\action_queue\ActionQueueItemStorage",
 *     "storage_schema" = "Drupal\action_queue\ActionQueueItemStorageSchema",
 *     "access" = "Drupal\action_queue\ActionQueueItemAccessControl",
 *     "list_builder" = "Drupal\action_queue\ActionQueueItemListBuilder",
 *     "views_data" = "Drupal\action_queue\ActionQueueItemViewsData",
 *     "form" = {
 *       "delete" = "Drupal\action_queue\Form\ActionQueueItemDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "details" = "Drupal\action_queue\Form\ActionQueueItemDetailsForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "entity_type" = "entity_type",
 *     "entity_id" = "entity_id",
 *     "action" = "action",
 *     "uid" = "uid",
 *     "status" = "status",
 *     "created" = "created",
 *   },
 *   links = {
 *     "delete-form" = "/action_queue_item/{action_queue_item}/delete",
 *     "delete-multiple-form" = "/action_queue_item/delete",
 *     "details-form" = "/action_queue_item/{action_queue_item}/details",
 *   }
 * )
 */
class ActionQueueItem extends ContentEntityBase implements ActionQueueItemInterface {

  use EntityChangedTrait;

  /**
   * Get the label of the action.
   *
   * @return string
   *   The message of the action.
   */
  public function label() {
    if (!empty($this->getMessage())) {
      return $this->getMessage();
    }
    return $this->getAction();
  }

  /**
   * {@inheritdoc}
   */
  public function getAction() {
    return $this->get('action')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    $value = $this->get('value')->value;
    if (!empty($value)) {
      return unserialize($this->get('value')->value);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value) {
    $this->set('value', serialize($value));
  }

  /**
   * {@inheritdoc}
   */
  public function getTriggerArguments() {
    $value = $this->get('trigger_value')->value;
    if (!empty($value)) {
      return unserialize($this->get('trigger_value')->value);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setTriggerArguments($data) {
    $this->set('trigger_value', serialize($data));
  }

  /**
   * {@inheritdoc}
   */
  public function setMessage(string $message) {
    $this->set('message', $message);
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->get('message')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setViolationMessage(string $message) {
    $this->set('violation_message', $message);
  }

  /**
   * {@inheritdoc}
   */
  public function getViolationMessage() {
    return $this->get('violation_message')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getActionPlugin() {
    $action = \Drupal::service('plugin.manager.action')->createInstance($this->get('action')->value);
    $value = ($this->getValue()) ? $this->getValue() : [];
    $action->setConfiguration($value);

    $trigger_values = ($this->getTriggerArguments()) ? $this->getTriggerArguments() : [];
    $action->getTrigger()->setConfiguration($trigger_values);
    return $action;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferencedEntity() {
    return $this->entityTypeManager()->getStorage($this->get('entity_type')->value)->load($this->get('entity_id')->value);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id']->setLabel(new TranslatableMarkup('ID'))
      ->setDescription(new TranslatableMarkup('The ID.'));

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Entity Type'))
      ->setDescription(new TranslatableMarkup('The target entity type.'));

    $fields['entity_id'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Entity ID'))
      ->setDescription(new TranslatableMarkup('The ID of the entity of which this action is acting upon.'))
      ->setSetting('max_length', 255);

    $fields['action'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Action'))
      ->setDescription(new TranslatableMarkup('The action plugin ID.'));

    $fields['value'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Value'))
      ->setDescription(new TranslatableMarkup('An optional value to be passed.'));

    $fields['trigger_value'] = BaseFieldDefinition::create('string_long')
      ->setLabel(new TranslatableMarkup('Trigger values'))
      ->setDescription(new TranslatableMarkup('Optional values to be used during trigger evaluation.'));

    $fields['message'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Message'))
      ->setDescription(new TranslatableMarkup('An optional message to be stored.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The user ID of author of the action.'))
      ->setSettings([
        'target_type' => 'user',
        'default_value' => 0,
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active status'))
      ->setDescription(t('Whether action is active or inactive.'))
      ->setDefaultValue(TRUE);
    $fields['status']->getItemDefinition()->setClass(StatusItem::class);

    $fields['action_state'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Action state'))
      ->setDescription(new TranslatableMarkup('The current action state'));

    $fields['violation_message'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Violations'))
      ->setDescription(new TranslatableMarkup('Any violation messages.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['executed'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Executed'))
      ->setDescription(t('The time that the action was executed.'));

    return $fields;
  }

}
