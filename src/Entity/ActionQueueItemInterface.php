<?php

namespace Drupal\action_queue\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Interface ActionQueueItem entities.
 *
 * @package Drupal\action_queue\Entity
 */
interface ActionQueueItemInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Get the action.
   *
   * @return string
   *   The action plugin id.
   */
  public function getAction();

  /**
   * Set the status of the action queue item.
   *
   * @param int $status
   *   The status integer.
   */
  public function setStatus($status);

  /**
   * Get the status of the action queue item.
   *
   * @return int
   *   The status integer.
   */
  public function getStatus();

  /**
   * Get the action value argument.
   *
   * @return mixed
   *   The value that gets passed to the action when it is executed.
   */
  public function getValue();

  /**
   * Set the action value argument.
   *
   * @param $value
   *   The value that gets passed to the action when it is executed.
   */
  public function setValue($value);

  /**
   * Get the trigger arguments.
   *
   * @return array
   *   Array of arguments that get used during trigger evaluation.
   */
  public function getTriggerArguments();

  /**
   * Set the trigger arguments.
   *
   * @param $data
   *   Array of arguments that get used during trigger evaluation.
   */
  public function setTriggerArguments($data);

  /**
   * Retrieve message.
   *
   * @return string
   *   The message.
   */
  public function getMessage();

  /**
   * Set the message.
   *
   * @param string $message
   *   The message.
   */
  public function setMessage(string $message);

  /**
   * Retrieve the violation message.
   *
   * @return string
   *   The message.
   */
  public function getViolationMessage();

  /**
   * Set the violation message.
   *
   * @param string $message
   *   The message.
   */
  public function setViolationMessage(string $message);

  /**
   * Get the action plugin.
   *
   * @return \Drupal\action_queue\Plugin\ActionQueueActionInterface
   *   The action plugin with hydrated values from this entity.
   */
  public function getActionPlugin();

  /**
   * Get the referenced entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity that is referenced by this item.
   */
  public function getReferencedEntity();

}
