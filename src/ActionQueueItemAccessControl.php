<?php

namespace Drupal\action_queue;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the action queue item entity.
 *
 * @see \Drupal\action_queue\Entity\ActionQueueItem
 */
class ActionQueueItemAccessControl extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation == 'delete' && $entity->isNew()) {
      return AccessResult::forbidden()->addCacheableDependency($entity);
    }
    if ($operation == 'delete') {
      return AccessResult::allowedIfHasPermission($account, 'delete action queue item');
    }
    if ($operation == 'status') {
      return AccessResult::allowedIfHasPermission($account, 'change action queue item status');
    }
    else {
      // No opinion.
      return AccessResult::neutral();
    }
  }

}
