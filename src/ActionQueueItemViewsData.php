<?php

namespace Drupal\action_queue;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Action Queue Item entities.
 */
class ActionQueueItemViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
