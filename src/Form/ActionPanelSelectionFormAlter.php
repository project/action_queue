<?php

namespace Drupal\action_queue\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\action_queue\ActionWaitQueue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\SubformState;

/**
 * Alter the Action Panel Selection form.
 *
 * This adds compatibility with Action Panel Form submission and Action Queue
 * plugins.
 */
class ActionPanelSelectionFormAlter implements ContainerInjectionInterface {

  /**
   * The action wait queue.
   *
   * @var \Drupal\action_queue\ActionWaitQueue
   */
  protected $actionWaitQueue;

  /**
   * ActionPanelSelectionFormAlter constructor.
   *
   * @param \Drupal\action_queue\ActionWaitQueue $action_wait_queue
   *   The action wait queue service.
   */
  public function __construct(ActionWaitQueue $action_wait_queue) {
    $this->actionWaitQueue = $action_wait_queue;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('action_queue.wait_queue')
    );
  }

  /**
   * Alters the action panel selection form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @see hook_form_alter()
   */
  public function formAlter(array &$form, FormStateInterface $form_state) {
    $form_object = $form_state->getFormObject();
    $plugin = $form_object->plugin();
    if ($this->actionWaitQueue->isSupportedAction($plugin)) {
      $trigger = $plugin->getTrigger();
      $form['trigger'] = [
        '#type' => 'container',
        '#weight' => -100,
      ];
      // Creating a subformstate in order to keep things isolated.
      // This prevents other form_state variables from the form being passed
      // into the trigger form.
      $subform_state = SubformState::createForSubform($form['trigger'], $form, $form_state);
      $form['trigger'] = $trigger->buildConfigurationForm($form['trigger'], $subform_state);

      // Reset the form submission entirely. This is to ensure that no
      // actions get executed immediately.
      $form['#submit'] = ['Drupal\action_queue\Form\ActionPanelSelectionFormAlter::submitForm'];
    }
  }

  /**
   * Form submit callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function submitForm(array $form, FormStateInterface $form_state) {
    // @todo some kind of error is being triggered.
    $object = $form_state->getFormObject();
    $actionWaitQueue = \Drupal::service('action_queue.wait_queue');
    $plugin = $object->plugin();

    if ($actionWaitQueue->isSupportedAction($plugin)) {

      $subform_state = SubformState::createForSubform($form['values'], $form, $form_state);
      $plugin->submitConfigurationForm($form['values'], $subform_state);
      $values = $plugin->prepareConfiguration($subform_state->getValues(), $form['values'], $subform_state);
      $plugin->setConfiguration($values);

      if (!empty($form['trigger'])) {
        $subform_state = SubformState::createForSubform($form['trigger'], $form, $form_state);
        $plugin->getTrigger()->submitConfigurationForm($form['trigger'], $subform_state);
      }

      $entities = $object->getSelectedEntities($form, $form_state);
      if (!is_array($entities)) {
        $entities = [$entities];
      }
      $enqueued = 0;
      foreach ($entities as $entity) {
        // Do a final check to make sure that this action is applicable before
        // adding it to the queue.
        $class = get_class($plugin);
        // @todo need a better way to handle context.
        $context = ['entity' => $entity];
        if ($class::isApplicable($context)) {
          $actionWaitQueue->addItem($plugin, $entity);
          $enqueued++;
        }
      }

      if ($enqueued) {
        $definition = $plugin->getPluginDefinition();
        $message = t('Action: <em>@name</em> added @count item(s) to the queue.', ['@count' => $enqueued, '@name' => $definition['label']]);
        \Drupal::messenger()->addMessage($message);
      }
      else {
        \Drupal::messenger()->addWarning(t('No items were added to the queue.'));
      }
    }
  }

}
