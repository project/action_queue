<?php

namespace Drupal\action_queue\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting ActionQueueItem entities.
 *
 * @ingroup action_queue
 */
class ActionQueueItemDeleteForm extends ContentEntityDeleteForm {

}
