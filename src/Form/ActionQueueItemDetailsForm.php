<?php

namespace Drupal\action_queue\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Url;

/**
 * Provides a form for showing ActionQueueItem details.
 *
 * @ingroup action_queue
 */
class ActionQueueItemDetailsForm extends ContentEntityForm {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a ActionQueueItemDetailsForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, DateFormatterInterface $date_formatter, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->dateFormatter = $date_formatter;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'action_queue_item_details_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $date = $this->dateFormatter->format($entity->get('created')->value, 'custom', 'D, d/m/y - h:i:s');
    $user = $this->entityTypeManager->getStorage('user')->load($entity->get('uid')->target_id);
    $target = $entity->getReferencedEntity();

    $rows[] = $this->buildRow('Action', $this->getActionLabel($entity));
    $rows[] = $this->buildRow('Created', $date);
    $rows[] = $this->buildRow('User', $user->toLink());
    $rows[] = $this->buildRow('Entity', $target->toLink());
    $rows[] = $this->buildRow('Message', $entity->getMessage());
    $rows[] = $this->buildRow('Violations', $entity->getViolationMessage());

    $form['details_table'] = [
      '#type' => 'table',
      '#rows' => $rows,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['back'] = [
      '#type' => 'link',
      '#title' => $this->t('Back'),
      '#url' => $this->getRedirectUrl(\Drupal::request()),
      '#attributes' => [
        'class' => [
          'button',
        ],
      ],
      '#weight' => 10,
    ];
    return $form;
  }

  /**
   * Get the label of the action plugin.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The action queue item.
   *
   * @return string
   *   The action plugin label.
   */
  protected function getActionLabel(EntityInterface $entity) {
    $action = $entity->getAction();
    try {
      $definition = \Drupal::service('plugin.manager.action')->getDefinition($action);
      return $definition['label'];
    }
    catch (PluginNotFoundException $e) {
      return $action;
    }
  }

  /**
   * Build the table row.
   *
   * @param string $label
   *   The row label.
   * @param mixed $data
   *   The rendered data.
   *
   * @return array
   *   The table row.
   */
  public function buildRow($label, $data) {
    return [
      ['data' => $label, 'header' => TRUE],
      ['data' => $data],
    ];
  }

  /**
   * Get the redirect url.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Drupal\Core\Url|null
   *   The redirect url.
   */
  public function getRedirectUrl(Request $request) {
    // Prepare cancel link.
    $query = $request->query;
    $url = NULL;
    // If a destination is specified, that serves as the cancel link.
    if ($query->has('destination')) {
      $options = UrlHelper::parse($query->get('destination'));
      // @todo Revisit this in https://www.drupal.org/node/2418219.
      try {
        $url = Url::fromUserInput('/' . ltrim($options['path'], '/'), $options);
      }
      catch (\InvalidArgumentException $e) {
        // Suppress the exception and fall back to the form's cancel url.
      }
    }
    return $url;
  }

}
