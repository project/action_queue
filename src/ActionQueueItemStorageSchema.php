<?php

namespace Drupal\action_queue;

use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;

/**
 * Schema handler for the action_panel_data entity.
 *
 * @see \Drupal\action_queue\Entity\ActionQueueItem
 */
class ActionQueueItemStorageSchema extends SqlContentEntityStorageSchema {

}
