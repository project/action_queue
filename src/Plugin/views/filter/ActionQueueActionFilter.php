<?php

namespace Drupal\action_queue\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Action\ActionManager;

/**
 * Provides a select filter for action queue actions.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("action_queue_action_filter")
 */
class ActionQueueActionFilter extends InOperator {

  /**
   * {@inheritdoc}
   */
  protected $valueFormType = 'select';

  /**
   * The action plugin manager.
   *
   * @var \Drupal\Core\Action\ActionManager
   */
  protected $actionManager;

  /**
   * Creates an instance of ActionQueueActionFilter.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ActionManager $action_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->actionManager = $action_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.action'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {

    if (!isset($this->valueOptions)) {
      $this->valueOptions = [];

      // Get all the action queue action plugins.
      $definitions = $this->getActionQueueActionDefinitions();
      $options = [];
      foreach ($definitions as $definition) {
        $options[$definition['id']] = $definition['label'];
      }

      asort($options);
      $this->valueOptions = $options;
    }

    return $this->valueOptions;
  }

  /**
   * Get action queue supported plugins.
   *
   * @return array
   *   An array of action queue plugin definitions.
   */
  private function getActionQueueActionDefinitions() {
    // @todo create decorator.
    $definitions = $this->actionManager->getDefinitions();;
    $action_queue_definitions = [];
    foreach ($definitions as $definition) {
      if (isset($definition['action_queue']['trigger'])) {
        $action_queue_definitions[] = $definition;
      }
    }

    return $action_queue_definitions;
  }

}
