<?php

namespace Drupal\action_queue\Plugin\views\field;

use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\Core\Action\ActionManager;

/**
 * Field handler to display action plugin label.
 *
 * @ViewsField("action_label")
 */
class ActionLabel extends FieldPluginBase {

  /**
   * Array of loaded action labels.
   *
   * @var array
   */
  protected $actionLabels = [];

  /**
   * The action plugin manager.
   *
   * @var \Drupal\Core\Action\ActionManager
   */
  protected $actionManager;

  /**
   * Constructs an ActionLabel object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Action\ActionManager $action_manager
   *   The action plugin manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ActionManager $action_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->actionManager = $action_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.action')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);

    if (!isset($this->actionLabels[$value])) {
      return;
    }

    return $this->sanitizeValue($this->actionLabels[$value]);
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$values) {
    // This loads all the labels into an associative array
    // before rendering them.
    foreach ($values as $value) {
      $action = $this->getValue($value);
      if (!isset($this->actionLabels[$action])) {
        $definition = $this->actionManager->getDefinition($action);
        if (!empty($definition)) {
          $this->actionLabels[$action] = $definition['label'];
        }
      }
    }
  }

}
