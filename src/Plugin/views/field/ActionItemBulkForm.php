<?php

namespace Drupal\action_queue\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;

/**
 * Defines an action queue item operations bulk form element.
 *
 * @ViewsField("action_item_bulk_form")
 */
class ActionItemBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No action items selected.');
  }

}
