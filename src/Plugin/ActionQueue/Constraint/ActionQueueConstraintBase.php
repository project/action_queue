<?php

namespace Drupal\action_queue\Plugin\ActionQueue\Constraint;

use Drupal\action_queue\ActionExecutionContextInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for action constraint plugins.
 *
 * Use constraint plugins with your actions when you want to set additional
 * conditions around when they should execute.
 *
 * @see plugin_api
 */
abstract class ActionQueueConstraintBase extends PluginBase implements ActionQueueConstraintInterface {

  /**
   * The current entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The current context.
   *
   * @var \Drupal\action_queue\ActionExecutionContextInterface
   */
  protected $context;

  /**
   * Constructs an Action Constraint Base object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Set the configuration option.
   *
   * @param $key
   *   The configuration key.
   * @param $value
   *   The configuration value.
   */
  public function setOption($key, $value) {
    $this->configuration[$key] = $value;
  }

  /**
   * Get the configuration option.
   *
   * @param $key
   *   The configuration key.
   *
   * @return mixed|void
   *   The configuration value.
   */
  public function getOption($key) {
    if (isset($this->configuration[$key])) {
      return $this->configuration[$key];
    }
  }

  /**
   * Get the current configuration options.
   *
   * @return array
   *   The array of configuration options.
   */
  public function getOptions() {
    return $this->configuration;
  }

  /**
   *  Get the context.
   *
   * @return \Drupal\action_queue\ActionExecutionContextInterface
   *   The context.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Set the current context.
   *
   * @param \Drupal\action_queue\ActionExecutionContextInterface $context
   *   The context.
   */
  public function setContext(ActionExecutionContextInterface $context) {
    $this->context = $context;
  }

  /**
   * Set the current entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Return the current entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {

  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {

  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata() {
    return [];
  }

}
