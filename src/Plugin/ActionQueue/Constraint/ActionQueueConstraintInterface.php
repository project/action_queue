<?php

namespace Drupal\action_queue\Plugin\ActionQueue\Constraint;

/**
 * Defines the interface for action constraint plugins.
 *
 * @see \Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintBase
 * @see plugin_api
 */
interface ActionQueueConstraintInterface {

  /**
   * Validate the constraint.
   *
   * @return bool
   *   TRUE if it passes, FALSE if it fails.
   */
  public function validate();

  /**
   * Return a message when the validation fails.
   *
   * @return string
   *   The validation message.
   */
  public function getMessage();

  /**
   * Return any valid metadata associated with the validation.
   *
   * @return array
   *   The validation metadata.
   */
  public function getMetadata();

}
