<?php

namespace Drupal\action_queue\Plugin\ActionQueue\Trigger;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Defines the interface for action queue trigger plugins.
 *
 * @package Drupal\action_queue\Plugin\ActionQueue\Trigger
 */
interface ActionQueueTriggerInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Prepare values that will be saved to the database as arguments.
   *
   * @param array $values
   *   The submitted values.
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state interface.
   *
   * @return array
   *   Array of prepared values.
   */
  public function prepareConfiguration($values, $form, FormStateInterface $form_state);

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   * @param array $arguments
   *   Arguments as created from prepareConfiguration().
   *
   * @return bool
   *   TRUE of trigger passes, FALSE otherwise.
   *
   */
  public function evaluate(EntityInterface $entity, $arguments = []);

}
