<?php

namespace Drupal\action_queue\Plugin\Action;

use Drupal\action_queue\ActionQueueConstraintManagerInterface;
use Drupal\action_queue\ActionQueueConstraintValidatorInterface;
use Drupal\action_queue\ActionQueueExecutionContext;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Validates action queue constraints.
 *
 * @Action(
 *   id = "action_queue_validate_action",
 *   label = @Translation("Validate items"),
 *   type = "action_queue_item",
 * )
 */
class ValidateActionConstraints extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The action queue constraint plugin manager.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintManagerInterface
   */
  protected $constraintManager;

  /**
   * The action queue constraint validator service.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintValidatorInterface
   */
  protected $constraintValidator;

  /**
   * ValidateActionConstraints constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\action_queue\ActionQueueConstraintManagerInterface $constraint_manager
   *   The action queue constraint plugin manager.
   * @param \Drupal\action_queue\ActionQueueConstraintValidatorInterface $constraint_validator
   *   The action queue constraint validator.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ActionQueueConstraintManagerInterface $constraint_manager, ActionQueueConstraintValidatorInterface $constraint_validator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->constraintManager = $constraint_manager;
    $this->constraintValidator = $constraint_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.action_constraint'),
      $container->get('action_queue.constraint_validator'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute(EntityInterface $entity = NULL) {
    $action = $entity->getActionPlugin();
    $subject_entity = $this->entityTypeManager->getStorage($entity->get('entity_type')->value)->load($entity->get('entity_id')->value);

    if (!empty($subject_entity)) {
      $constraints = $this->constraintManager->getConstraintsByAction($action);
      $this->constraintValidator->initialize(new ActionQueueExecutionContext($action->getPluginId(), $entity, $subject_entity, 'bulk_form'));
      $this->constraintValidator->validateConstraints($constraints);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $account->hasPermission('validate queued actions');
  }

}
