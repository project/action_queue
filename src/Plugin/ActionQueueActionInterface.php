<?php

namespace Drupal\action_queue\Plugin;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines the interface for action panel action plugins.
 *
 * @see \Drupal\action_queue\Plugin\ActionQueueActionBase
 * @see plugin_api
 */
interface ActionQueueActionInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface, ConfigurableInterface {

  /**
   * Determine whether the action should be available based on context.
   *
   * @param array $context
   *   The current context.
   *
   * @return bool
   *   True if it is applicable, false otherwise.
   */
  public static function isApplicable(array $context);

  /**
   * Message to describe the action performed when the plugin is executed.
   *
   * @param null|\Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function getMessage($entity = NULL);

  /**
   * Executes the action.
   *
   * @param null|\Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  public function execute($entity = NULL);

}
