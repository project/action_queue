<?php

namespace Drupal\action_queue\Plugin;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for action queue plugins.
 *
 * @see \Drupal\action_queue\Plugin\ActionQueueActionInterface
 * @see plugin_api
 */
abstract class ActionQueueActionBase extends ActionBase implements ActionQueueActionInterface {

  /**
   * Configuration information passed into the plugin.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * The trigger plugin.
   *
   * @var \Drupal\action_queue\Plugin\ActionQueue\Trigger\ActionQueueTriggerInterface
   */
  protected $trigger;

  /**
   * Constructs a ActionPanelActionBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(array $context = []): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public function getMessage($entity = NULL) {

  }

  /**
   * Get the current trigger plugin (if applicable).
   *
   * @return \Drupal\action_queue\Plugin\ActionQueue\Trigger\ActionQueueTriggerInterface
   *   The trigger plugin or FALSE if not applicable.
   */
  public function getTrigger() {
    // @todo this needs to be done with an action plugin decorator.
    if (empty($this->trigger)) {
      $definition = $this->getPluginDefinition();
      if (isset($definition['action_queue']['trigger'])) {
        $plugin = $definition['action_queue']['trigger'];
        $this->trigger = \Drupal::service('plugin.manager.action_queue_trigger')->createInstance($plugin);
      }
    }
    return $this->trigger;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return AccessResult::allowed();
  }

}
