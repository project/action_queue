<?php

namespace Drupal\action_queue;

use Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface;

/**
 * Provides an Action constraint validator service.
 *
 * @see plugin_api
 */
interface ActionQueueConstraintValidatorInterface {

  /**
   * Initialize the validator service.
   *
   * @param \Drupal\action_queue\ActionExecutionContextInterface $context
   *   The action execution context.
   */
  public function initialize(ActionExecutionContextInterface $context);

  /**
   * Validate the constraint plugin.
   *
   * @param \Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface
   *   The action constraint plugin.id.
   */
  public function validate(ActionQueueConstraintInterface $constraint);

  /**
   * Validates an action against all constraints assigned to it.
   *
   * @param \Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface[] $constraints
   *   The constraints which should be ensured for the given value.
   */
  public function validateConstraints($constraints);

  /**
   * Return whether constraints were valid or not.
   *
   * @return bool
   *   TRUE if all constraints passed, FALSE otherwise.
   */
  public function isValid();

  /**
   * Get the action execution context.
   *
   * @return \Drupal\action_queue\ActionExecutionContextInterface
   *   The execution context.
   */
  public function getContext();

}
