<?php

namespace Drupal\action_queue;

use Drupal\Core\Action\ActionInterface;
use Drupal\Core\Action\ActionManager;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\action_queue\Entity\ActionQueueItemInterface;
use Drupal\action_queue\Event\AddActionToQueueEvent;
use Drupal\action_queue\Event\RemoveActionFromQueueEvent;
use Drupal\action_queue\Plugin\ActionQueueActionInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Drupal\action_queue\Event\ActionQueueEvents;

/**
 * Action wait queue service.
 *
 * @package Drupal\action_queue
 */
class ActionWaitQueue implements ActionWaitQueueInterface {

  /**
   * The action plugin manager service.
   *
   * @var \Drupal\Core\Action\ActionManager
   */
  protected $actionPluginManager;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * An event dispatcher.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Action constraint plugin manager.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintManagerInterface
   */
  protected $actionConstraintPluginManager;

  /**
   * Action constraint validator service.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintValidatorInterface
   */
  protected $actionConstraintValidator;

  /**
   * Stores which items have been triggered.
   *
   * @var array
   */
  protected $triggered = [];

  /**
   * Constructs an ActionWaitQueue object.
   *
   * @param \Drupal\Core\Action\ActionManager $action_plugin_manager
   *   The action plugin manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   An event dispatcher instance to use for configuration events.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param ActionQueueConstraintManagerInterface $action_constraint_manager
   *   The action constraint plugin manager.
   * @param ActionQueueConstraintValidatorInterface $action_constraint_validator
   *   The action constraint validator.
   */
  public function __construct(ActionManager $action_plugin_manager, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, TimeInterface $time, ActionQueueConstraintManagerInterface $action_constraint_manager, ActionQueueConstraintValidatorInterface $action_constraint_validator) {
    $this->actionPluginManager = $action_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->time = $time;
    $this->actionConstraintPluginManager = $action_constraint_manager;
    $this->actionConstraintValidator = $action_constraint_validator;
  }

  /**
   * {@inheritdoc}
   */
  public function isSupportedAction(ActionInterface $plugin) {
    $definition = $plugin->getPluginDefinition();
    if ($plugin instanceof ActionQueueActionInterface && isset($definition['action_queue']['trigger'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareAction($action, array $values = [], array $trigger_arguments = []) {
    $action = $this->actionPluginManager->createInstance($action);
    $action->setConfiguration($values);
    $action->getTrigger()->setConfiguration($trigger_arguments);
    return $action;
  }

  /**
   * {@inheritdoc}
   */
  public function addItem(ActionQueueActionInterface $plugin, EntityInterface $entity) {
    // Check to see if the plugin has a condition. If it does then save it
    // as an entity and don't execute it immediately.
    if (!$this->isSupportedAction($plugin)) {
      return;
    }

    $item = $this->entityTypeManager->getStorage('action_queue_item')->create([
      'uid' => \Drupal::currentUser()->id(),
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'action' => $plugin->getPluginId(),
      'status' => 1,
      'executed' => 0,
    ]);

    // If any arguments are passed then save them to the entity
    // to be evaluated against later.
    if (!empty($plugin->getConfiguration())) {
      $item->setValue($plugin->getConfiguration());
    }

    if ($plugin->getTrigger()) {
      $item->setTriggerArguments($plugin->getTrigger()->getConfiguration());
    }

    if ($message = $plugin->getMessage($entity)) {
      $item->setMessage($message);
    }
    $item->save();

    // Dispatch the AddActionToQueue event.
    $event = new AddActionToQueueEvent($plugin, $entity);
    $this->eventDispatcher->dispatch($event, ActionQueueEvents::ACTION_ADDED);
  }

  /**
   * {@inheritdoc}
   */
  public function removeItem(ActionQueueActionInterface $plugin, EntityInterface $entity) {
    // Check to see if the plugin has a condition. If it does then save it
    // as an entity and don't execute it immediately.
    if ($this->isSupportedAction($plugin)) {
      $actions = $this->entityTypeManager->getStorage('action_queue_item')->loadByProperties([
        'entity_type' => $entity->getEntityTypeId(),
        'entity_id' => $entity->id(),
        'action' => $plugin->getPluginId(),
      ]);
      if (!empty($actions)) {
        foreach ($actions as $action) {
          $action->delete();
          // Dispatch the RemoveActionFromQueue event.
          $event = new RemoveActionFromQueueEvent($plugin, $entity);
          $this->eventDispatcher->dispatch($event, ActionQueueEvents::ACTION_REMOVED);
        }
      }
    }
  }

  /**
   * TGet the action queue storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The action queue storage.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getStorage() {
    return $this->entityTypeManager->getStorage('action_queue_item');
  }

  /**
   * {@inheritdoc}
   */
  public function loadItem(ActionQueueActionInterface $plugin, EntityInterface $entity) {
    return $this->entityTypeManager->getStorage('action_queue_item')->loadByProperties([
      'action' => $plugin->getPluginId(),
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
    ]);

  }

  /**
   * {@inheritdoc}
   */
  public function clearExecutedItems() {
    $entity_ids = $this->entityTypeManager->getStorage('action_queue_item')->getQuery()
      ->condition('executed', 0, '!=')
      ->execute();
    if (!empty($entity_ids)) {
      $entities = $this->entityTypeManager->getStorage('action_queue_item')->loadMultiple($entity_ids);
      foreach ($entities as $entity) {
        $entity->delete();
      }
    }
  }

  /**
   * Fetch the actions by a certain trigger.
   *
   * @param string $trigger
   *   The trigger plugin mame.
   *
   * @return array
   *   The array of instantiated plugins.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getActionIdsByTrigger($trigger) {
    $plugins = [];
    $definitions = $this->actionPluginManager->getDefinitions();
    foreach ($definitions as $definition) {
      if (isset($definition['action_queue']['trigger'])) {
        if ($definition['action_queue']['trigger'] == $trigger) {
          $plugins[] = $definition['id'];
        }
      }
    }
    return $plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemsByEntity(EntityInterface $entity) {
    $queued_actions = \Drupal::entityTypeManager()->getStorage('action_queue_item')->loadByProperties([
      'executed' => 0,
      'entity_type' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
    ]);

    return $queued_actions;
  }

  /**
   * Reset triggered items.
   *
   * This can be helpful in rare cases where we need to attempt to execute an
   * action multiple times in a single request. This could happen if invoking
   * the same hook in different modules.
   */
  public function resetTriggeredItems() {
    $this->triggered = [];
  }

  /**
   * Add the triggered action to the cache.
   *
   * @param \Drupal\action_queue\Entity\ActionQueueItemInterface $item
   *   The action queue item.
   */
  private function addTriggeredItem(ActionQueueItemInterface $item) {
    $this->triggered[] = $item->id();
  }

  /**
   * Determine whether we've already triggered this item.
   *
   * @param \Drupal\action_queue\Entity\ActionQueueItemInterface $item
   *   The action queue item.
   *
   * @return bool
   *   TRUE if it has already been triggered, FALSE otherwise.
   */
  private function isTriggered(ActionQueueItemInterface $item) {
    if (in_array($item->id(), $this->triggered)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Execute an action queue item.
   *
   * @param \Drupal\action_queue\Entity\ActionQueueItemInterface $item
   *   The action queue item.
   */
  protected function executeAction(ActionQueueItemInterface $item) {
    // Load the entity associated with this item.
    $entity = $item->getReferencedEntity();
    $plugin = $item->getActionPlugin();

    if (!empty($entity) && $plugin->getTrigger()->evaluate($entity, $item->getTriggerArguments())) {
      // Check the action constraints pass before executing it.
      $constraints = $this->actionConstraintPluginManager->getConstraintsByAction($plugin);
      $this->actionConstraintValidator->initialize(new ActionQueueExecutionContext($plugin->getPluginId(), $item, $entity, 'execute'));
      $this->actionConstraintValidator->validateConstraints($constraints);

      // If constraints passed then execute the plugin.
      if ($this->actionConstraintValidator->isValid()) {
        $plugin->executeMultiple([$entity]);

        // Set to inactive and save the date of execution.
        $item->set('status', 0);
        $item->set('executed', $this->time->getRequestTime());
        $item->setViolationMessage('');
        $item->save();
      }
      else {
        // If there are any violation messages then add that to the item and
        // save it.
        $messages = $this->actionConstraintValidator->getContext()->getMessages();
        if (!empty($messages)) {
          $messages = implode(', ', $messages);
          $item->setViolationMessage($messages);
          $item->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function triggerActions($trigger, array $entities = []) {
    $action_ids = $this->getActionIdsByTrigger($trigger);
    // If no actions found then exit early.
    if (empty($action_ids)) {
      return;
    }
    // Only trigger actions that are active and have not been executed.
    $query = $this->entityTypeManager->getStorage('action_queue_item')->getQuery()
      ->accessCheck(FALSE)
      ->condition('action', $action_ids, 'IN')
      ->condition('status', 1)
      ->condition('executed', 0);

    // If entities are set then we should filter the query by those ids.
    // Otherwise allow it to act upon all the queue items.
    if (!empty($entities)) {
      foreach ($entities as $entity) {
        $entity_types[] = $entity->getEntityTypeId();
        $entity_ids[] = $entity->id();
      }
      $query->condition('entity_type', array_values(array_unique($entity_types)), 'IN');
      $query->condition('entity_id', array_values(array_unique($entity_ids)), 'IN');
    }
    $item_ids = $query->execute();

    if (!empty($item_ids)) {
      $queued_items = $this->entityTypeManager->getStorage('action_queue_item')->loadMultiple($item_ids);
      foreach ($queued_items as $item) {
        if (!$this->isTriggered($item)) {

          // This tracks which actions have been triggered or not. This helps
          // with performance issues.
          $this->addTriggeredItem($item);

          $this->executeAction($item);
        }
      }
    }
  }

}
