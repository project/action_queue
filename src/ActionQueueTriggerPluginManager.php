<?php

namespace Drupal\action_queue;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Class ActionQueueTriggerPluginManager.
 *
 * @package Drupal\action_queue
 */
class ActionQueueTriggerPluginManager extends DefaultPluginManager {

  /**
   * ActionQueueTriggerPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ActionQueue/Trigger', $namespaces, $module_handler, 'Drupal\action_queue\Plugin\ActionQueue\Trigger\ActionQueueTriggerInterface', 'Drupal\action_queue\Annotation\ActionQueueTrigger');

    $this->alterInfo('action_queue_trigger');
    $this->setCacheBackend($cache_backend, 'action_queue_trigger');
  }

}
