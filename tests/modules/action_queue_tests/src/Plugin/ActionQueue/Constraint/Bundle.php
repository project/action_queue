<?php

namespace Drupal\action_queue_tests\Plugin\ActionQueue\Constraint;

use Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface;
use Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintBase;

/**
 * Bundle constraint.
 *
 * @ActionQueueConstraint(
 *   id = "bundle",
 *   label = @Translation("Content is of a certain bundle"),
 * )
 */
class Bundle extends ActionQueueConstraintBase implements ActionQueueConstraintInterface {

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $entity = $this->getEntity();

    $options = $this->getOptions();
    $value = array_pop($options);
    if ($entity->bundle() == $value) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->t('This is not the right content type.');
  }

}
