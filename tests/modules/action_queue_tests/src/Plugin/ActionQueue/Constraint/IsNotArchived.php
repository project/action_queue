<?php

namespace Drupal\action_queue_tests\Plugin\ActionQueue\Constraint;

use Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintInterface;
use Drupal\action_queue\Plugin\ActionQueue\Constraint\ActionQueueConstraintBase;

/**
 * Archived content constraint.
 *
 * @ActionQueueConstraint(
 *   id = "is_not_archived",
 *   label = @Translation("If content is not archived"),
 * )
 */
class IsNotArchived extends ActionQueueConstraintBase implements ActionQueueConstraintInterface {

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $entity = $this->getEntity();

    if ($entity->get('moderation_state')->value != 'archived') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->t('This content is archived and cannot be published.');
  }

}
