<?php

namespace Drupal\action_queue_tests\Plugin\ActionQueue\Trigger;

use Drupal\action_queue\Plugin\ActionQueue\Trigger\ActionQueueTriggerBase;

/**
 * On date trigger.
 *
 * @ActionQueueTrigger(
 *   id = "on_date",
 *   admin_label = @Translation("On date"),
 * )
 */
class OnDate extends ActionQueueTriggerBase {

  /**
   * {@inheritdoc}
   */
  public function evaluate($entity, $arguments = []) {
    $current_time = \Drupal::time()->getCurrentTime();
    $archive_time = $arguments['date'];
    if ($current_time >= $archive_time) {
      return TRUE;
    }
    return FALSE;
  }

}
