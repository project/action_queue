<?php

namespace Drupal\action_queue_tests\Plugin\Action;

use Drupal\action_queue\Plugin\ActionQueueActionBase;

/**
 * Test action for action panel. This only outputs a message.
 *
 * @Action(
 *   id = "publish",
 *   label = @Translation("Publish the action."),
 *   category = @Translation("test"),
 *   type = "node",
 *   action_queue = {
 *     "trigger" = "on_date",
 *     "constraints" = {
 *        "is_not_archived" = {},
 *        "bundle" = {"page"}
 *     },
 *   }
 * )
 */
class PublishNodeActionTest extends ActionQueueActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    $entity->set('moderation_state', 'published');
    $entity->setPublished();
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage($entity = NULL) {
    return 'The node was published.';
  }

}
