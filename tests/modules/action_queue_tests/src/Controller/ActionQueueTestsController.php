<?php

namespace Drupal\action_queue_tests\Controller;

use Drupal\action_queue\Controller\ActionsQueuedListingControllerBase;

/**
 * A base class for listing action queue actions.
 *
 * @package Drupal\action_queue\Controller
 */
class ActionQueueTestsController extends ActionsQueuedListingControllerBase {

}
