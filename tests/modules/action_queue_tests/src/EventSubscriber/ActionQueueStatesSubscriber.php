<?php

namespace Drupal\action_queue_tests\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\action_queue\Event\ActionQueueConstraintsValidateEvent;

class ActionQueueStatesSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ActionQueueConstraintsValidateEvent::NAME => 'onValidate',
    ];
  }

  /**
   * Handle constraint validation.
   *
   * @param \Drupal\action_queue\Event\ActionQueueConstraintsValidateEvent $event
   *   The action queue constraint validate event.
   */
  public function onValidate(ActionQueueConstraintsValidateEvent $event) {
    if ($event->getContext()->getKey() == 'validate' && !$event->isValid()) {
      $item = $event->getContext()->getItem();
      $item->set('status', 0);
      $item->set('action_state', 'needs_review');
      $item->save();
    }
  }

}
