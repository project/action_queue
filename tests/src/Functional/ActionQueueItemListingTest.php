<?php

namespace Drupal\Tests\action_queue\Functional;

use Drupal\workflows\Entity\Workflow;

/**
 * Tests the action queue list builder.
 *
 * @group action_queue
 */
class ActionQueueItemListingTest extends ActionQueueFunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $workflow = Workflow::create([
      'type' => 'action_queue_states',
      'id' => 'action_states',
      'label' => 'Action States',
      'type_settings' => [
        'states' => [
          'active' => [
            'label' => 'Active',
            'weight' => 0,
            'active' => FALSE,
          ],
          'inactive' => [
            'label' => 'Inactive',
            'active' => FALSE,
            'weight' => 1,
          ],
          'needs_review' => [
            'label' => 'Needs review',
            'active' => FALSE,
            'weight' => 2,
          ],
          'failed' => [
            'label' => 'Failed',
            'active' => FALSE,
            'weight' => 4,
          ],
        ],
        'transitions' => [
          'pause' => [
            'label' => 'Pause',
            'from' => ['active'],
            'to' => 'inactive',
            'weight' => 0,
          ],
          'resume' => [
            'label' => 'Pause',
            'from' => ['inactive'],
            'to' => 'active',
            'weight' => 0,
          ],
        ],
        'actions' => [
          'publish' => 'publish',
        ],
      ],
    ]);
    $workflow->save();
  }

  /**
   * View the views page listing upcoming actions.
   */
  public function testViewActionsViewPage() {
    $entity = $this->drupalCreateNode([
      'title' => 'test node',
      'type' => 'page',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $this->actionWaitQueue->addItem($action, $entity);

    $web_user = $this->createUser([
      'view action queue item details',
      'delete action queue item',
      'change action queue item status',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('/actions');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->pageTextContains('Publish the action');

    $this->assertSession()->linkExists('Pause');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->linkExists('Details');
    $this->clickLink('Details');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->linkExists('Pause');
    $this->assertSession()->linkExists('Back');
    $this->clickLink('Back');

    $this->assertSession()->linkExists('Delete');
    $this->clickLink('Delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Basic test for the details page.
   */
  public function testViewActionDetailsPage() {
    $entity = $this->drupalCreateNode([
      'title' => 'test node',
      'type' => 'page',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $this->actionWaitQueue->addItem($action, $entity);

    $web_user = $this->createUser([
      'view action queue item details',
    ]);
    $this->drupalLogin($web_user);

    $this->drupalGet('/action_queue_item/1/details');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('View action queue item details');
    $this->assertSession()->pageTextContains('Publish the action');
    $this->assertSession()->pageTextContains('The node was published.');
    $this->assertSession()->pageTextContains('Active');
    $this->assertSession()->pageTextContains('Violations');
    $this->assertSession()->linkNotExists('Back');
  }

}
