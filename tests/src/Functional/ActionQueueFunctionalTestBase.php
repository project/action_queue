<?php

namespace Drupal\Tests\action_queue\Functional;

use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\user\Entity\User;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\BrowserTestBase;

/**
 * Action queue kernel test base class.
 */
abstract class ActionQueueFunctionalTestBase extends BrowserTestBase {

  use UserCreationTrait;
  use ContentModerationTestTrait;

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $testUser;

  /**
   * Set the default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'system',
    'options',
    'user',
    'filter',
    'text',
    'field',
    'datetime',
    'node',
    'workflows',
    'content_moderation',
    'action_queue',
    'action_queue_states',
    'action_queue_tests',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    NodeType::create(['type' => 'page', 'name' => 'Page'])->save();
    NodeType::create(['type' => 'article', 'name' => 'Article'])->save();
    ;
    $this->testUser = User::create([
      'name' => 'foobar1',
      'mail' => 'foobar1@example.com',
    ]);
    $this->testUser->save();

    $this->actionManager = $this->container->get('plugin.manager.action');
    $this->actionWaitQueue = $this->container->get('action_queue.wait_queue');
    $this->entityTypeManager = $this->container->get('entity_type.manager');
  }

}
