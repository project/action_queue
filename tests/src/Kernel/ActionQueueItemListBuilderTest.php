<?php

namespace Drupal\Tests\action_queue\Kernel;

use Drupal\action_queue\ActionQueueItemListBuilder;

/**
 * Tests the action queue list builder.
 *
 * @group action_queue
 */
class ActionQueueItemListBuilderTest extends ActionQueueKernelTestBase {

  /**
   * Basic test for the action queue list builder.
   */
  public function testActionQueueItemListBuilder() {
    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'page',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $this->actionWaitQueue->addItem($action, $entity);

    // Add items to the queue. Ensure that the list builder is rendering them.
    $entity_type = $this->entityTypeManager->getDefinition('action_queue_item');
    $listBuilder = ActionQueueItemListBuilder::createInstance($this->container,$entity_type);
    $data = $listBuilder->render();

    // Basic assertion that it's rendering the rows we're expecting.
    $count = count($data['table']['#rows']);
    $this->assertEquals(1, $count, 'It should render one row of action items');
  }

}
