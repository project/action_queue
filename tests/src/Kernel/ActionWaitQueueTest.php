<?php

namespace Drupal\Tests\action_queue\Kernel;

/**
 * Tests the action wait queue basic operations.
 *
 * @group action_queue
 */
class ActionWaitQueueTest extends ActionQueueKernelTestBase {

  /**
   * Tests the preparation of an action queue plugin.
   */
  public function testPrepareAction() {
    $trigger_args = ['date' => \Drupal::time()->getCurrentTime()];
    $action = $this->actionWaitQueue->prepareAction('publish', [], $trigger_args);

    $values = $action->getConfiguration();
    $this->assertSame([], $values);

    $trigger_arguments = $action->getTrigger()->getConfiguration();
    $this->assertSame($trigger_args, $trigger_arguments);
  }

  /**
   * Tests whether the action was added to the queue.
   */
  public function testAddItem() {
    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'page',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $this->actionWaitQueue->addItem($action, $entity);

    // Assert that it was added.
    $items = $this->entityTypeManager->getStorage('action_queue_item')->loadByProperties([
      'action' => 'publish',
      'entity_id' => $entity->id(),
      'entity_type' => $entity->getEntityTypeId(),
    ]);

    $this->assertNotEmpty($items, 'This should have saved an action queue item.');

  }

  /**
   * Tests whether the action was removed from the queue.
   */
  public function testRemoveItem() {
    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'page',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $this->actionWaitQueue->addItem($action, $entity);

    $this->actionWaitQueue->removeItem($action, $entity);

    // Assert that it was added.
    $items = $this->entityTypeManager->getStorage('action_queue_item')->loadByProperties([
      'action' => 'publish',
      'entity_id' => $entity->id(),
      'entity_type' => $entity->getEntityTypeId(),
    ]);

    $this->assertEmpty($items, 'This should have been removed from the action queue.');
  }

  /**
   * Tests the instantiation of an action plugin from an action queue item.
   */
  public function testGetActionPluginMethod() {

    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'page',
    ]);
    $original_action = $this->actionManager->createInstance('publish');
    $original_action->setConfiguration(['field' => 'test value']);
    $original_action->getTrigger()->setConfiguration(['trigger_field' => 'trigger value']);

    $this->actionWaitQueue->addItem($original_action, $entity);

    // Assert that the plugin returned from the data is the same as the
    // original.
    $item = $this->entityTypeManager->getStorage('action_queue_item')->load(1);

    $action = $item->getActionPlugin();

    $this->assertEquals($original_action->getConfiguration(), $action->getConfiguration(), 'The action configuration should match.');

    $this->assertEquals($original_action->getTrigger()->getConfiguration(), $action->getTrigger()->getConfiguration(), 'The trigger configuration should match.');

  }

  /**
   * Tests the retrieval of the referenced entity from the action queue item.
   */
  public function testGetReferencedEntityMethod() {

    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'page',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $action->setConfiguration(['field' => 'test value']);
    $action->getTrigger()->setConfiguration(['trigger_field' => 'trigger value']);

    $this->actionWaitQueue->addItem($action, $entity);

    // Assert that getReferencedEntity returns the correct entity.
    $item = $this->entityTypeManager->getStorage('action_queue_item')->load(1);
    $referenced_entity = $item->getReferencedEntity();

    $this->assertEquals($entity->getEntityTypeId(), $referenced_entity->getEntityTypeId(), 'The entity type should match.');
    $this->assertEquals($entity->id(), $referenced_entity->id(),'The entity id should match.');

  }

  /**
   * Tests that invalid triggers do not execute the actions.
   */
  public function testInvalidTriggerActionsMethod() {
    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'page',
      'status' => 0,
      'moderation_state' => 'draft',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $this->actionWaitQueue->addItem($action, $entity);

    // Trigger an invalid trigger plugin name. This should have no effect in
    // the system.
    $this->actionWaitQueue->triggerActions('invalid_trigger');

    // Assert that nothing changed in the database.
    $items = $this->entityTypeManager->getStorage('action_queue_item')->loadByProperties([
      'action' => 'publish',
    ]);
    foreach ($items as $item) {
      $this->assertEquals(0, $item->get('executed')->value, 'Invalid trigger name - Action queue items should not have been executed');
    }

  }

  /**
   * Tests that valid triggers execute the actions.
   */
  public function testValidTriggerActionsMethod() {
    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'page',
      'status' => 0,
      'moderation_state' => 'draft',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $action->getTrigger()->setConfiguration([
      'date' => \Drupal::time()->getCurrentTime() - 86400,
    ]);
    $this->actionWaitQueue->addItem($action, $entity);
    $this->actionWaitQueue->triggerActions('on_date');

    $items = $this->actionWaitQueue->loadItem($action, $entity);
    foreach ($items as $item) {
      $this->assertEquals(0, $item->getStatus(), 'Action queue item should be inactive after execution.');
      $this->assertGreaterThan(0, $item->get('executed')->value, 'Action queue item execution date should be greater than 0.');

      // Confirm that the content has been published.
      $node = $item->getReferencedEntity();
      $this->assertEquals(1, $node->get('status')->value, 'The node should have been published');
    }
  }

}
