<?php

namespace Drupal\Tests\action_queue\Kernel;

/**
 * Tests the action wait queue with action constraints.
 *
 * In this test the action queue items will always remain active however the
 * node should change. To see action queue items change see the
 * action_queue_states tests.
 *
 * @group action_queue
 */
class ActionWaitQueueConstraintsTest extends ActionQueueKernelTestBase {

  /**
   * Create content fixtures.
   */
  public function createActionQueueItemFixtures() {
    // These two nodes should be published after execution.
    $fixtures[] = [
      'action' => 'publish',
      'node' => [
        'nid' => 1,
        'title' => 'test 1',
        'type' => 'page',
        'status' => 0,
        'moderation_state' => 'draft',
      ],
      'expected' => [
        'node' => [
          'status' => 1,
          'moderation_state' => 'published',
        ],
        'action_queue_item' => [
          'status' => 0,
          'violation_message' => '',
        ],
      ],
    ];
    $fixtures[] = [
      'action' => 'publish',
      'node' => [
        'nid' => 2,
        'title' => 'test 2',
        'type' => 'page',
        'status' => 0,
        'moderation_state' => 'draft',
      ],
      'expected' => [
        'node' => [
          'status' => 1,
          'moderation_state' => 'published',
        ],
        'action_queue_item' => [
          'status' => 0,
          'violation_message' => '',
        ],
      ],
    ];
    // This should trigger the IsNotArchived constraint which will prevent it
    // from being published.
    $fixtures[] = [
      'action' => 'publish',
      'node' => [
        'nid' => 3,
        'title' => 'test 4',
        'type' => 'page',
        'status' => 0,
        // It should only publish non archived nodes.
        'moderation_state' => 'archived',
      ],
      'expected' => [
        'node' => [
          'status' => 0,
          'moderation_state' => 'archived',
        ],
        'action_queue_item' => [
          'status' => 1,
          'violation_message' => 'This content is archived and cannot be published.',
        ],
      ],
    ];
    // This should trigger the Bundle constraint which will prevent it from
    // being published.
    $fixtures[] = [
      'action' => 'publish',
      'node' => [
        'nid' => 4,
        'title' => 'test 5',
        // It should only publish page nodes.
        'type' => 'article',
        'status' => 0,
        'moderation_state' => 'draft',
      ],
      'expected' => [
        'node' => [
          'status' => 0,
          'moderation_state' => 'draft',
        ],
        'action_queue_item' => [
          'status' => 1,
          'violation_message' => 'This is not the right content type.',
        ],
      ],
    ];

    foreach ($fixtures as $fixture) {
      $entity = $this->createStubNode($fixture['node']);
      $entity->save();

      $action = $this->actionManager->createInstance($fixture['action']);
      $action->getTrigger()->setConfiguration([
        'date' => \Drupal::time()->getCurrentTime() - 86400,
      ]);
      $this->actionWaitQueue->addItem($action, $entity);
    }

    return $fixtures;
  }

  /**
   * Does a more advanced trigger test against constraints.
   */
  public function testTriggerActionsMethodWithConstraints() {
    // Move this to its own test.
    $fixtures = $this->createActionQueueItemFixtures();
    $this->actionWaitQueue->triggerActions('on_date');

    // Assert that the final status of the action queue items and nodes
    // matches what is defined in the fixture.
    foreach ($fixtures as $fixture) {
      $node = $this->entityTypeManager->getStorage('node')->load($fixture['node']['nid']);
      $expected = $fixture['expected'];
      $this->assertEquals($expected['node']['status'], $node->get('status')->value, 'The status for node ' . $node->id() . ' does not match');
      $this->assertEquals($expected['node']['moderation_state'], $node->get('moderation_state')->value, 'The moderation state for node ' . $node->id() . ' does not match');

      $items = $this->entityTypeManager->getStorage('action_queue_item')->loadByProperties([
        'action' => $fixture['action'],
        'entity_type' => $node->getEntityTypeId(),
        'entity_id' => $node->id(),
      ]);
      foreach ($items as $item) {
        $this->assertEquals($expected['action_queue_item']['status'], $item->getStatus(), 'Action queue item status for node ' . $fixture['node']['nid'] . ' is incorrect');
        $this->assertEquals($expected['action_queue_item']['violation_message'], $item->getViolationMessage());
      }
    }
  }

}
