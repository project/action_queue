<?php

namespace Drupal\Tests\action_queue\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\User;
use Drupal\node\Entity\NodeType;

/**
 * Action queue kernel test base class.
 */
abstract class ActionQueueKernelTestBase extends KernelTestBase {

  use UserCreationTrait;
  use ContentModerationTestTrait;

  /**
   * Disable schema validation when running tests.
   *
   * @var bool
   *
   * @todo Fix this by providing actual schema validation.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * User for testing.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $testUser;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'system',
    'options',
    'user',
    'filter',
    'text',
    'field',
    'datetime',
    'node',
    'workflows',
    'content_moderation',
    'action_queue',
    'action_queue_tests',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['system', 'field', 'node', 'filter', 'text']);
    $this->installSchema('system', ['sequences']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('content_moderation_state');
    $this->installConfig('content_moderation');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('action_queue_item');

    NodeType::create(['type' => 'page', 'name' => 'Page'])->save();
    NodeType::create(['type' => 'article', 'name' => 'Article'])->save();
    ;
    $this->testUser = User::create([
      'name' => 'foobar1',
      'mail' => 'foobar1@example.com',
    ]);
    $this->testUser->save();

    $this->actionManager = $this->container->get('plugin.manager.action');
    $this->actionWaitQueue = $this->container->get('action_queue.wait_queue');
    $this->entityTypeManager = $this->container->get('entity_type.manager');

    $workflow = $this->createEditorialWorkflow();
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'page');
    $workflow->getTypePlugin()->addEntityTypeAndBundle('node', 'article');
    $workflow->save();

    \Drupal::service('current_user')->setAccount($this->testUser);
  }

  /**
   * Creates a stub node with the given values.
   *
   * @param array $values
   *   The node values to create the node with.
   *
   * @return \Drupal\node\NodeInterface
   *   The created node, which has been saved.
   *
   * @throws \Exception
   */
  protected function createStubNode(array $values) {
    $node = $this->container->get('entity_type.manager')->getStorage('node')
      ->create($values + [
        'title' => rand(),
      ]);
    $node->save();
    return $node;
  }

}
