<?php

namespace Drupal\Tests\action_queue\Kernel;

use Drupal\action_queue\Entity\ActionQueueItemInterface;
use Drupal\workflows\Entity\Workflow;
use Drupal\action_queue\ActionQueueExecutionContext;

/**
 * Tests the action queue states.
 *
 * @group action_queue
 */
class ActionQueueStatesTest extends ActionQueueKernelTestBase {

  /**
   * The wait queue.
   *
   * @var \Drupal\action_queue\ActionWaitQueueInterface
   */
  protected $waitQueue;

  /**
   * The constraint plugin manager.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintManagerInterface
   */
  protected $constraintManager;

  /**
   * The constraint validator.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintValidatorInterface
   */
  protected $constraintValidator;

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = [
    'system',
    'options',
    'user',
    'filter',
    'text',
    'field',
    'datetime',
    'node',
    'workflows',
    'content_moderation',
    'action_queue',
    'action_queue_states',
    'action_queue_tests',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->waitQueue = $this->container->get('action_queue.wait_queue');
    $this->constraintValidator = $this->container->get('action_queue.constraint_validator');
    $this->constraintManager = $this->container->get('plugin.manager.action_constraint');

    $workflow = Workflow::create([
      'type' => 'action_queue_states',
      'id' => 'action_states',
      'label' => 'Action States',
      'type_settings' => [
        'states' => [
          'active' => [
            'label' => 'Active',
            'weight' => 0,
            'active' => FALSE,
          ],
          'inactive' => [
            'label' => 'Inactive',
            'active' => FALSE,
            'weight' => 1,
          ],
          'needs_review' => [
            'label' => 'Needs review',
            'active' => FALSE,
            'weight' => 2,
          ],
          'failed' => [
            'label' => 'Failed',
            'active' => FALSE,
            'weight' => 4,
          ],
        ],
        'transitions' => [
          'pause' => [
            'label' => 'Pause',
            'from' => ['active'],
            'to' => 'inactive',
            'weight' => 0,
          ],
          'resume' => [
            'label' => 'Pause',
            'from' => ['inactive'],
            'to' => 'active',
            'weight' => 0,
          ],
        ],
        'actions' => [
          'publish' => 'publish',
        ],
      ],
    ]);
    $workflow->save();
  }

  /**
   * Validate constraints for a specific item.
   *
   * @param \Drupal\action_queue\Entity\ActionQueueItemInterface $item
   *   The action queue item.
   */
  protected function validateConstraints(ActionQueueItemInterface $item) {
    $entity = $item->getReferencedEntity();
    $plugin = $item->getActionPlugin();

    if (!empty($entity)) {
      $this->constraintValidator->initialize(new ActionQueueExecutionContext($plugin->getPluginId(), $item, $entity, 'validate'));
      $constraints = $this->constraintManager->getConstraintsByAction($plugin);
      $this->constraintValidator->validateConstraints($constraints);
    }
  }

  /**
   * Tests that invalid action queue items are marked as needs_review.
   */
  public function testActiveToNeedsReviewActions() {
    // This will fail and should trigger the ActionQueueStatesSubscriber.
    $entity = $this->createStubNode([
      'title' => 'test node',
      'type' => 'article',
      'status' => 0,
      'moderation_state' => 'draft',
    ]);
    $action = $this->actionManager->createInstance('publish');
    $this->actionWaitQueue->addItem($action, $entity);

    // Validate the queued action, this should set the action queue item to
    // needs_review.
    $items = $this->waitQueue->getItemsByEntity($entity);
    foreach ($items as $item) {
      $this->assertEquals(1, $item->get('status')->value);
      $this->validateConstraints($item);
    }

    // The items should be flagged as needs_review.
    $items = $this->waitQueue->getItemsByEntity($entity);
    foreach ($items as $item) {
      $this->assertEquals(0, $item->get('status')->value);
      $this->assertEquals('needs_review', $item->get('action_state')->value);
    }
  }

}
