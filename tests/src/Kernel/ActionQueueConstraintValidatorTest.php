<?php

namespace Drupal\Tests\action_queue\Kernel;

use Drupal\action_queue\ActionExecutionContext;

/**
 * Tests the action queue constraint validator.
 *
 * @group action_queue
 */
class ActionQueueConstraintValidatorTest extends ActionQueueKernelTestBase {

  /**
   * The constraint plugin manager.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintManagerInterface
   */
  protected $constraintManager;

  /**
   * The constraint validator.
   *
   * @var \Drupal\action_queue\ActionQueueConstraintValidatorInterface
   */
  protected $constraintValidator;

  /**
   * Create content fixtures.
   */
  public function createActionQueueItemFixtures() {
    // These two nodes should be published after execution.
    $fixtures[] = [
      'action' => 'publish',
      'node' => [
        'id' => 1,
        'title' => 'test 1',
        'type' => 'article',
        'status' => 0,
        'moderation_state' => 'draft',
      ],
      'expected' => [
        'violations' => ['bundle'],
        'pass' => FALSE,
      ],
    ];

    foreach ($fixtures as $fixture) {
      $entity = $this->createStubNode($fixture['node']);
      $entity->save();
    }

    return $fixtures;
  }

  /**
   * Does a more advanced trigger test against constraints.
   */
  public function testActionQueueConstraintValidator() {
    // Move this to its own test.
    $this->constraintManager = $this->container->get('plugin.manager.action_constraint');
    $this->constraintValidator = $this->container->get('action_queue.constraint_validator');

    $constraint_plugins = [
      'bundle' => ['page'],
      'is_not_archived' => [],
    ];

    $constraints = [];
    foreach ($constraint_plugins as $id => $options) {
      $constraints[] = $this->constraintManager->create($id, $options);
    }

    // Assert that the final status of the action queue items and nodes
    // matches what is defined in the fixture.
    $fixtures = $this->createActionQueueItemFixtures();
    foreach ($fixtures as $fixture) {
      $node = $this->entityTypeManager->getStorage('node')->load($fixture['node']['id']);

      $action = 'test';
      $this->constraintValidator->initialize(new ActionExecutionContext($action, $node));
      $this->constraintValidator->validateConstraints($constraints);

      // This is a static cache test to ensure that the constraints don't
      // get validated more than once for each context.
      $this->constraintValidator->validateConstraints($constraints);

      $context = $this->constraintValidator->getContext();

      $expected = $fixture['expected'];
      $this->assertEquals($expected['violations'], $context->getViolations(), 'The violations do not match.');

      $this->assertEquals($expected['pass'], $this->constraintValidator->isValid(), 'The validation result does not match.');
    }
  }

}
