<?php

namespace Drupal\action_queue_states\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Plugin\WorkflowTypeStateFormBase;
use Drupal\workflows\StateInterface;

/**
 * The action queue states form.
 *
 * @see \Drupal\content_moderation\Plugin\WorkflowType\ContentModeration
 */
class ActionQueueStatesForm extends WorkflowTypeStateFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, StateInterface $state = NULL) {
    /** @var \Drupal\content_moderation\ContentModerationState $state */
    $state = $form_state->get('state');
    $is_required_state = isset($state) ? in_array($state->id(), $this->workflowType->getRequiredStates(), TRUE) : FALSE;

    $form = [];
    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#description' => $this->t('When content reaches this state it should be active.'),
      '#default_value' => isset($state) ? $state->isActiveState() : FALSE,
      '#disabled' => $is_required_state,
    ];

    return $form;
  }

}
