<?php

namespace Drupal\action_queue_states\Form;

use Drupal\Core\Action\ActionManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\workflows\Plugin\WorkflowTypeConfigureFormBase;
use Drupal\workflows\State;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The content moderation WorkflowType configuration form.
 *
 * @see \Drupal\content_moderation\Plugin\WorkflowType\ContentModeration
 */
class ActionQueueStatesConfigureForm extends WorkflowTypeConfigureFormBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The action plugin manager.
   *
   * @var ActionManager
   */
  protected $actionManager;

  /**
   * Create an instance of ContentModerationConfigureForm.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ActionManager $actionManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->actionManager = $actionManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.action'),
    );
  }

  /**
   * Get action queue supported plugins.
   *
   * @param array $definition
   *   The action plugin definition.
   *
   * @return array
   *   An array of action queue plugin definitions.
   */
  public function getActionQueueActionDefinitions() {
    // @todo create decorator.
    $definitions = $this->actionManager->getDefinitions();;
    $action_queue_definitions = [];
    foreach ($definitions as $definition) {
      if (isset($definition['action_queue']['trigger'])) {
        $action_queue_definitions[] = $definition;
      }
    }

    return $action_queue_definitions;
  }

  /**
   * Create an array of action plugin options.
   *
   * @param array $plugin_ids
   *   The array of action plugin ids.
   *
   * @return array
   *   An associative array of plugin options.
   */
  protected function getAvailableActionPluginOptions(EntityInterface $entity) {
    $workflows = $this->entityTypeManager->getStorage('workflow')->loadByProperties(['type' => 'action_queue_states']);
    $selected = [];
    foreach ($workflows as $workflow) {
      if ($workflow->id() != $entity->id()) {
        $configuration = $workflow->getTypePlugin()->getConfiguration();
        $actions = array_filter($configuration['actions']);
        foreach ($actions as $action) {
          if (!in_array($action, $selected)) {
            $selected[] = $action;
          }
        }
      }
    }

    $definitions = $this->getActionQueueActionDefinitions();
    $options = [];
    foreach ($definitions as $definition) {
      if (!in_array($definition['id'], $selected)) {
        $options[$definition['id']] = $definition['label'];
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->workflowType->getConfiguration();
    $form['actions'] = [
      '#type' => 'details',
      '#title' => $this->t('Actions'),
      '#open' => TRUE,
    ];

    $options = $this->getAvailableActionPluginOptions($form_state->getFormObject()->getEntity());
    if (!empty($options)) {
      $form['actions']['#description'] = $this->t('Assign workflow to these actions. Unchecked actions will always remain active. Note that actions can only be assigned to one workflow.');
      $form['actions']['action_queue_plugins'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Actions'),
        '#options' => $options,
        '#default_value' => $configuration['actions'] ?? [],
      ];
    }
    else {
      $form['actions']['message'] = [
        '#type' => 'markup',
        '#markup' => $this->t('There are currently no actions available.'),
      ];
    }

    $form['workflow_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Workflow Settings'),
      '#open' => TRUE,
    ];
    $form['workflow_settings']['default_action_state'] = [
      '#title' => $this->t('Default action state'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => array_map([State::class, 'labelCallback'], $this->workflowType->getStates()),
      '#description' => $this->t('Select the state that new action queue items will be assigned.'),
      '#default_value' => $configuration['default_action_state'] ?? 'active',
    ];
    $form['workflow_settings']['executed_action_state'] = [
      '#title' => $this->t('Executed action state'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => array_map([State::class, 'labelCallback'], $this->workflowType->getStates()),
      '#description' => $this->t('Select the state that action queue items will be assigned after they are executed.'),
      '#default_value' => $configuration['executed_action_state'] ?? 'inactive',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $configuration = $this->workflowType->getConfiguration();
    $configuration['default_action_state'] = $form_state->getValue(['workflow_settings', 'default_action_state']);
    $configuration['executed_action_state'] = $form_state->getValue(['workflow_settings', 'executed_action_state']);
    if (!empty($form_state->getValue(['actions','action_queue_plugins']))) {
      $configuration['actions'] = $form_state->getValue(['actions', 'action_queue_plugins']);
    }
    $this->workflowType->setConfiguration($configuration);
  }

}
