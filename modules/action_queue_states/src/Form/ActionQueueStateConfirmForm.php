<?php

namespace Drupal\action_queue_states\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for setting the status of ActionQueueItem entities.
 *
 * @ingroup action_queue_states
 */
class ActionQueueStateConfirmForm extends ContentEntityConfirmFormBase {

  /**
   * The current transition.
   *
   * @var \Drupal\workflows\TransitionInterface
   */
  protected $transition;

  /**
   * Get the current transition.
   *
   * @return \Drupal\workflows\TransitionInterface
   *   The current transition.
   */
  public function getTransition() {
    if (empty($this->transition)) {
      $transition = $this->getRouteMatch()->getParameter('transition');
      $workflow = \Drupal::service('action_queue_states.information')->getWorkflowForEntity($this->getEntity());
      $this->transition = $workflow->getTypePlugin()->getTransition($transition);
    }

    return $this->transition;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $new_state = $this->getTransition()->to();
    if (!$new_state->isActiveState()) {
      return $this->t('This will deactivate the action.');
    }
    return $this->t('This will activate the the action.');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to @transition this action?', ['@transition' => $this->getTransition()->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    \Drupal::service('action_queue_states.transitions')->invokeTransition($this->getTransition(), $this->getEntity());
  }

}
