<?php

namespace Drupal\action_queue_states;

use Drupal\Core\Entity\EntityInterface;
use Drupal\action_queue\ActionQueueItemListBuilder;

/**
 * Builds a listing of action queue item entities.
 *
 * @ingroup action_queue
 */
class ActionQueueStatesItemListBuilder extends ActionQueueItemListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getStatus(EntityInterface $entity) {
    // Get the action state label.
    if (!empty($entity->get('action_state')->value)) {
      $workflow = \Drupal::service('action_queue_states.information')->getWorkflowForEntity($entity);
      if (!empty($workflow)) {
        $label = $workflow->getTypePlugin()->getState($entity->get('action_state')->value)->label();
        if (!empty($label)) {
          return $label;
        }
      }
    }

    return parent::getStatus($entity);
  }

}
