<?php

namespace Drupal\action_queue_states;

use Drupal\workflows\StateInterface;

/**
 * A value object representing a workflow state for action queue items.
 */
class ActionQueueState implements StateInterface {

  /**
   * The vanilla state object from the Workflow module.
   *
   * @var \Drupal\workflows\StateInterface
   */
  protected $state;

  /**
   * If entities should be active if in this state.
   *
   * @var bool
   */
  protected $active;

  /**
   * ActionQueueState constructor.
   *
   * @param \Drupal\workflows\StateInterface $state
   *   The vanilla state object from the Workflow module.
   * @param bool $active
   *   (optional) TRUE if action queue items should be active in this state,
   *   FALSE if not. Defaults to FALSE.
   */
  public function __construct(StateInterface $state, $active = FALSE) {
    $this->state = $state;
    $this->active = $active;
  }

  /**
   * Determines if actions should be active if in this state.
   *
   * @return bool
   *   return TRUE if it is in active state, FALSE otherwise.
   */
  public function isActiveState() {
    return $this->active;
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->state->id();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->state->label();
  }

  /**
   * {@inheritdoc}
   */
  public function weight() {
    return $this->state->weight();
  }

  /**
   * {@inheritdoc}
   */
  public function canTransitionTo($to_state_id) {
    return $this->state->canTransitionTo($to_state_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitionTo($to_state_id) {
    return $this->state->getTransitionTo($to_state_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getTransitions() {
    return $this->state->getTransitions();
  }

}
