<?php

namespace Drupal\action_queue_states;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\action_queue\Entity\ActionQueueItemInterface;

/**
 * General service for action state related questions.
 */
class ActionQueueStateInformation {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new ActionQueueStateInformation instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the current workflow.
   *
   * @param \Drupal\action_queue\Entity\ActionQueueItemInterface $entity
   *   The action queue item entity.
   *
   * @return \Drupal\workflows\WorkflowTypeInterface
   *   The workflow config entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getWorkflowForEntity(ActionQueueItemInterface $entity) {
    $action = $entity->get('action')->value;
    $workflows = $this->entityTypeManager->getStorage('workflow')->loadByProperties(['type' => 'action_queue_states']);
    foreach ($workflows as $workflow) {
      $settings = $workflow->get('type_settings');
      if (in_array($action, array_filter($settings['actions']))) {
        return $workflow;
      }
    }
  }

  /**
   * Return the current state for the action queue item.
   *
   * @param \Drupal\action_queue\Entity\ActionQueueItemInterface $entity
   *   The action queue item.
   *
   * @return string
   *   The current action state.
   */
  public function getCurrentState(ActionQueueItemInterface $entity) {
    if (!empty($entity->get('action_state')->value)) {
      return $entity->get('action_state')->value;
    }

    $workflow = $this->getWorkflowForEntity($entity);
    return $workflow->getTypePlugin()->getInitialState($entity)->id();
  }

}
