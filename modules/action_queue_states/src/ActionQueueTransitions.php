<?php

namespace Drupal\action_queue_states;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\workflows\TransitionInterface;

/**
 * Validates whether a certain state transition is allowed.
 */
class ActionQueueTransitions {

  /**
   * Action queue state information service.
   *
   * @var \Drupal\action_queue_states\ActionQueueStateInformation
   */
  protected $actionQueueStateInfo;

  /**
   * Constructs a new ActionQueueTransitions object.
   *
   * @param \Drupal\action_queue_states\ActionQueueStateInformation $action_queue_state_info
   *   The action queue state information service.
   */
  public function __construct(ActionQueueStateInformation $action_queue_state_info) {
    // @todo interface for action queue state.
    $this->actionQueueStateInfo = $action_queue_state_info;
  }

  /**
   * {@inheritdoc}
   */
  public function getValidTransitions(EntityInterface $entity, AccountInterface $user) {
    $workflow = $this->actionQueueStateInfo->getWorkflowForEntity($entity);
    // If no workflows are found that means it wasn't configured for this
    // action.
    if (empty($workflow)) {
      return [];
    }

    $entity_state = $this->actionQueueStateInfo->getCurrentState($entity);
    $current_state = $workflow->getTypePlugin()->getState($entity_state);

    // Returning all the valid transition.
    // @todo Add support for permissions.
    return $current_state->getTransitions();
  }

  /**
   * Invoke a transition on an action queue item.
   *
   * @param \Drupal\workflows\TransitionInterface $transition
   *   The transition.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function invokeTransition(TransitionInterface $transition, EntityInterface $entity) {
    $new_state = $transition->to();
    $entity->set('status', (int) $new_state->isActiveState());
    $entity->set('action_state', $new_state->id());
    $entity->save();
  }

}
