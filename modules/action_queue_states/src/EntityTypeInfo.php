<?php

namespace Drupal\action_queue_states;

use Drupal\action_queue_states\Form\ActionQueueStateConfirmForm;

/**
 * Manipulates entity type information.
 *
 * This class contains primarily bridged hooks for compile-time or
 * cache-clear-time hooks. Runtime hooks should be placed in EntityOperations.
 *
 * @internal
 */
class EntityTypeInfo {

  /**
   * Adds action queue states configuration to action queue items..
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The master entity type list to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types) {
    $entity_type = $entity_types['action_queue_item'];
    // Set the action state transition confirmation form.
    $entity_type->setFormClass('status', ActionQueueStateConfirmForm::class);
    $entity_type->setLinkTemplate('status-form', '/action_queue_item/{action_queue_item}/status/{transition}');

    // Set the action states item list builder.
    $entity_type->setListBuilderClass(ActionQueueStatesItemListBuilder::class);

  }

}
