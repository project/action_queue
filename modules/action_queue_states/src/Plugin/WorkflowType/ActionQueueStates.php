<?php

namespace Drupal\action_queue_states\Plugin\WorkflowType;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\workflows\Plugin\WorkflowTypeBase;
use Drupal\action_queue_states\ActionQueueState;

/**
 * Attaches workflows to action queue item entities.
 *
 * @WorkflowType(
 *   id = "action_queue_states",
 *   label = @Translation("Action Queue States"),
 *   required_states = {
 *     "active",
 *     "inactive",
 *   },
 *   forms = {
 *     "configure" = "\Drupal\action_queue_states\Form\ActionQueueStatesConfigureForm",
 *     "state" = "\Drupal\action_queue_states\Form\ActionQueueStatesForm",
 *   },
 * )
 */
class ActionQueueStates extends WorkflowTypeBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getActions() {
    return array_keys($this->configuration['actions']);
  }

  /**
   * {@inheritdoc}
   */
  public function getState($state_id) {
    $state = parent::getState($state_id);
    if (isset($this->configuration['states'][$state->id()]['active'])) {
      $state = new ActionQueueState($state, $this->configuration['states'][$state->id()]['active']);
    }
    else {
      $state = new ActionQueueState($state);
    }
    return $state;
  }

  /**
   * {@inheritdoc}
   */
  public function appliesToAction($action_id) {
    // @todo not currently being used.
    return in_array($action_id, $this->getActionPlugins(), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'states' => [
        'active' => [
          'label' => 'Active',
          'active' => TRUE,
          'weight' => 0,
        ],
        'inactive' => [
          'label' => 'Inactive',
          'active' => FALSE,
          'weight' => 1,
        ],
      ],
      'transitions' => [
        'resume' => [
          'label' => 'Resume',
          'to' => 'active',
          'weight' => 0,
          'from' => [
            'inactive',
          ],
        ],
        'pause' => [
          'label' => 'Pause',
          'to' => 'inactive',
          'weight' => 1,
          'from' => [
            'active',
          ],
        ],
      ],
      'actions' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    $configuration = parent::getConfiguration();
    // Ensure that states and entity types are ordered consistently.
    ksort($configuration['states']);
    return $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getInitialState($entity = NULL) {
    return $this->getState('active');
  }

}
