<?php

namespace Drupal\action_queue_states\Plugin\Field\FieldFormatter;

use Drupal\action_queue_states\ActionQueueStateInformation;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'content_moderation_state' formatter.
 *
 * @FieldFormatter(
 *   id = "action_queue_state",
 *   label = @Translation("Action state"),
 *   field_types = {
 *     "string",
 *   }
 * )
 */
class ActionQueueStateFormatter extends FormatterBase {

  /**
   * The action queue state information service.
   *
   * @var \Drupal\action_queue_states\ActionQueueStateInformation
   */
  protected $stateInformation;

  /**
   * Create an instance of ContentModerationStateFormatter.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ActionQueueStateInformation $state_information) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->stateInformation = $state_information;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('action_queue_states.information')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $workflow = $this->stateInformation->getWorkflowForEntity($items->getEntity());
    foreach ($items as $delta => $item) {
      // Actions won't always be assigned a workflow. We should fall back
      // to the raw field value in order to avoid an error.
      if (!empty($workflow)) {
        $label = $workflow->getTypePlugin()->getState($item->value)->label();
      }
      else {
        $label = $item->value;
      }
      $elements[$delta] = [
        '#markup' => $label,
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName() === 'action_state';
  }

}
