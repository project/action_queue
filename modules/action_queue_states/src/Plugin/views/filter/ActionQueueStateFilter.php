<?php

namespace Drupal\action_queue_states\Plugin\views\filter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\DependentWithRemovalPluginInterface;
use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter for the action queue state.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("action_queue_state_filter")
 */
class ActionQueueStateFilter extends InOperator implements DependentWithRemovalPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $valueFormType = 'select';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The storage handler of the workflow entity type.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $workflowStorage;

  /**
   * Creates an instance of ctionQueueStateFilter.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityStorageInterface $workflow_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->workflowStorage = $workflow_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), $this->entityTypeManager->getDefinition('workflow')->getListCacheTags());
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), $this->entityTypeManager->getDefinition('workflow')->getListCacheContexts());
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if (isset($this->valueOptions)) {
      return $this->valueOptions;
    }
    $this->valueOptions = [];

    // Get all the workflow options.
    foreach ($this->workflowStorage->loadByProperties(['type' => 'action_queue_states']) as $workflow) {
      /** @var \Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface $workflow_type */
      $workflow_type = $workflow->getTypePlugin();
      foreach ($workflow_type->getStates() as $state_id => $state) {
        $this->valueOptions[$workflow->label()][implode('-', [$workflow->id(), $state_id])] = $state->label();
      }
    }

    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple() {
    if (empty($this->value)) {
      return;
    }

    $this->ensureMyTable();

    $value = array_map(function ($item) {
      [$workflow_id, $state_id] = explode('-', $item, 2);
      return $state_id;
    }, $this->value);

    $this->query->addWhere($this->options['group'],"$this->tableAlias.$this->realField", $value, $this->operator);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();

    if ($workflow_ids = $this->getWorkflowIds()) {
      /** @var \Drupal\workflows\WorkflowInterface $workflow */
      foreach ($this->workflowStorage->loadMultiple($workflow_ids) as $workflow) {
        $dependencies[$workflow->getConfigDependencyKey()][] = $workflow->getConfigDependencyName();
      }
    }

    return $dependencies;
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(array $dependencies) {
    // See if this handler is responsible for any of the dependencies being
    // removed. If this is the case, indicate that this handler needs to be
    // removed from the View.
    $remove = FALSE;
    // Get all the current dependencies for this handler.
    $current_dependencies = $this->calculateDependencies();
    foreach ($current_dependencies as $group => $dependency_list) {
      // Check if any of the handler dependencies match the dependencies being
      // removed.
      foreach ($dependency_list as $config_key) {
        if (isset($dependencies[$group]) && array_key_exists($config_key, $dependencies[$group])) {
          // This handlers dependency matches a dependency being removed,
          // indicate that this handler needs to be removed.
          $remove = TRUE;
          break 2;
        }
      }
    }
    return $remove;
  }

  /**
   * Gets the list of Workflow IDs configured for this filter.
   *
   * @return array
   *   And array of workflow IDs.
   */
  protected function getWorkflowIds() {
    $workflow_ids = [];
    foreach ((array) $this->value as $value) {
      [$workflow_id] = explode('-', $value, 2);
      $workflow_ids[] = $workflow_id;
    }

    return array_unique($workflow_ids);
  }

}
