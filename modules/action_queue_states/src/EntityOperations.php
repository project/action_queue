<?php

namespace Drupal\action_queue_states;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\workflows\TransitionInterface;
use Drupal\Core\Url;

/**
 * Defines a class for reacting to entity events.
 *
 * @internal
 */
class EntityOperations implements ContainerInjectionInterface {

  use RedirectDestinationTrait;

  /**
   * The Action Queue Transitions Service.
   *
   * @var \Drupal\action_queue_states\ActionQueueTransitions
   */
  protected $transitions;

  /**
   * The action queue information service.
   *
   * @var \Drupal\action_queue_states\ActionQueueStateInformation
   */
  protected $actionQueueInfo;

  /**
   * Constructs a new EntityOperations object.
   *
   * @param \Drupal\action_queue_states\ActionQueueTransitions $transitions
   *   Action Queue Transitions service.
   * @param \Drupal\action_queue_states\ActionQueueStateInformation $action_queue_info
   *   The action queue information service.
   */
  public function __construct(ActionQueueTransitions $transitions, ActionQueueStateInformation $action_queue_info) {
    $this->transitions = $transitions;
    $this->actionQueueInfo = $action_queue_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('action_queue_states.transitions'),
      $container->get('action_queue_states.information'),
    );
  }

  /**
   * Create the transition operations links.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return array
   *   Array of available transition links.
   */
  public function entityOperations(EntityInterface $entity) {
    $valid = $this->transitions->getValidTransitions($entity, \Drupal::currentUser());
    $operations = [];
    if ($entity->hasLinkTemplate('status-form')) {
      foreach ($valid as $transition) {
        $status_link = $this->createTransitionLink($transition, $entity);
        $operations['status_' . $transition->id()] = $status_link;
      }
    }

    return $operations;
  }

  /**
   * Creates the transition link.
   *
   * @param \Drupal\workflows\TransitionInterface $transition
   *   The transition.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return array
   *   The transition link render array.
   */
  public function createTransitionLink(TransitionInterface $transition, EntityInterface $entity) {
    $options = [
      'action_queue_item' => $entity->id(),
      'transition' => $transition->id(),
    ];

    $url = Url::fromRoute('entity.action_queue_item.status_form', $options);
    return [
      'title' => $transition->label(),
      'url' => $this->ensureDestination($url),
      'weight' => 10,
      'access' => $entity->access('status', NULL, TRUE),
    ];
  }

  /**
   * Ensures that a destination is present on the given URL.
   *
   * @param \Drupal\Core\Url $url
   *   The URL object to which the destination should be added.
   *
   * @return \Drupal\Core\Url
   *   The updated URL object.
   */
  protected function ensureDestination(Url $url) {
    return $url->mergeOptions(['query' => $this->getRedirectDestination()->getAsArray()]);
  }

  /**
   * Alter the form_action_queue_item_details form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $form_id
   *   The form id.
   */
  public function formActionQueueItemDetailsFormAlter(array &$form, FormStateInterface $form_state, $form_id) {
    $entity = $form_state->getFormObject()->getEntity();

    $form['details_table']['#rows'][] = $this->buildRow('Status', $this->getCurrentState($entity));

    $valid = $this->transitions->getValidTransitions($entity, \Drupal::currentUser());
    if ($entity->hasLinkTemplate('status-form')) {
      foreach ($valid as $transition) {
        $form['actions']['status_' . $transition->id()] = $this->buildActionLink($transition, $entity);
      }
    }
  }

  /**
   * Get the current state of the action queue item.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The action queue state.
   */
  protected function getCurrentState(EntityInterface $entity) {
    $state = $this->actionQueueInfo->getCurrentState($entity);
    $workflow = $this->actionQueueInfo->getWorkflowForEntity($entity);
    return $workflow->getTypePlugin()->getState($state)->label();
  }

  /**
   * Build the table row.
   *
   * @param string $label
   *   The row label.
   * @param mixed $data
   *   The rendered data.
   *
   * @return array
   *   The table row.
   */
  public function buildRow($label, $data) {
    return [
      ['data' => $label, 'header' => TRUE],
      ['data' => $data],
    ];
  }

  /**
   * Build the actions link.
   *
   * This will appear on the bottom of the details page.
   *
   * @param \Drupal\workflows\TransitionInterface $transition
   *   The transition.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity.
   *
   * @return array
   *   The action link render array.
   */
  protected function buildActionLink(TransitionInterface $transition, EntityInterface $entity) {
    $options = [
      'action_queue_item' => $entity->id(),
      'transition' => $transition->id(),
    ];

    $url = Url::fromRoute('entity.action_queue_item.status_form', $options);

    return [
      '#type' => 'link',
      '#title' => $transition->label(),
      '#url' => $this->ensureDestination($url),
      '#access' => $entity->access('status', NULL, TRUE),
      '#attributes' => [
        'class' => [
          'button',
          'button--primary',
        ],
      ],
    ];
  }

}
