<?php

/**
 * Implements hook_views_data_alter()
 */
function action_queue_states_views_data_alter(array &$data) {
  // Changing the action state filter to a select list.
  $data['action_queue_data']['action_state']['filter'] = [
    'id' => 'action_queue_state_filter',
    'allow empty' => TRUE,
  ];
}
