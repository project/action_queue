INTRODUCTION
------------
The Action Queue States module provides states for Action Queue Items.

Major Concepts
------------
### Active State versus Inactive State

* Active State: An Action Queue Item that has an active state will execute
  when the trigger fires.
  * Examples of active state include: Ready, Waiting, Pending
* Inactive State: An Action Queue Item that has an inactive state will not
  execute when the trigger fires.
  * Examples of inactive state include: Paused, Failed, Blocked, Suspended, Error

HOW TO USE THIS MODULE
------------
1. Enable this module.
2. Create a new Action Queue States workflow.
   1. Go to Configuration > Workflows > Add workflow
3. Add whatever states and transitions you like.
4. Choose the Action Queue plugin(s) you want to use.

Example Use Case
------------
You can use this module with constraints to get some very powerful functionality.

For example, let's say you have some content that you've scheduled to be archived.
However, you have a constraint which prevents it because some parent content is
not yet archived.

Without states, your action is going to fail silently. However, with states,
you can change that Action Queue Item to "Failed", find that failed item, and take
steps to resolve it.

To do this, Action Queue provides an `ActionQueueConstraintsValidateEvent` to
respond to failed (and passed) constraints.

```php
class ArchiveFailedSubscriber implements EventSubscriberInterface {

  /**
   * The action wait queue.
   *
   * @var \Drupal\action_queue\ActionWaitQueueInterface
   */
  protected $actionWaitQueue;

  /**
   * The action constraint validation subscriber constructor.
   *
   * @param \Drupal\action_queue\ActionWaitQueueInterface $action_wait_queue
   *   The action wait queue.
   */
  public function __construct(ActionWaitQueueInterface $action_wait_queue) {
    $this->actionWaitQueue = $action_wait_queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ActionQueueConstraintsValidateEvent::NAME => 'onArchiveFail',
    ];
  }

  /**
   * Change the action state to "failed" when it fails.
   *
   * @param \Drupal\action_queue\Event\ActionQueueConstraintsValidateEvent $event
   *   The action constraint validate event.
   */
  public function onArchiveFail(ActionQueueConstraintsValidateEvent $event) {
    $item = $event->getContext()->getItem();
    if ($item->get('action')->value == 'archive_content') {
      if (!$event->isValid()) {
        $item->set('action_state', 'failed');
        $item->save();
      }
    }
  }

}


```
