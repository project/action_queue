INTRODUCTION
------------
The Action Queue module allows you to perform actions on entities in response
to triggers.

### What are some examples of how I could use this module?
* I want to send an email the next time a node is published.
* I want to publish a node on a specific date
* I want to delete all the comments when the node is archived. But only for a
  particular node.
* I want to archive a node on a specific date, but only if it hasn't been
  updated within the last 30 days.

### This is a developer module.
This module does not provide any user interface. This is a robust set of
plugins and services for developers to use to build out their features.

### How is this different from Drupal events or hooks?
Events and hooks are great for performing persistent operations under certain
conditions. However, it's impossible to **constrain an operation to a single
entity while being independent of that entity’s state.**

For example, with events and hooks, it’s very easy to send an email _every time_
a node is published. It merely needs to see if the node is published every
time it's saved. However, this gets difficult when you only want to send an
email _once._ It can't rely the node's published state because that will always
be true. Instead, it needs to rely on something else.

That "_something else_" is where it can get complicated and that is the gap
that Action Queue attempts to fill.

Major Concepts
------------
### Action Queue Action:

This is Drupal core's Action plugin. Actions are some operation that you want
to perform on an entity.

All action queue actions support configuration. This means you can pass values
along with your actions. For example, if you have a "Set Moderation State"
action, the configuration might be "Archive" or "Publish".

Important: This should _not_ be confused with Drupal configuration. These are
more like arguments that get passed in during execution.

### Action Queue Trigger

This is the heart of this module.

An action queue trigger tells the system _when_ to execute your action. Action
Queue Triggers also support configuration. For example, if you have an
"On Date" trigger, your configuration might be "180 days." If you use this
with your "Set Moderation State" action, you can schedule content to be
archived in 180 days.

### Action Constraint

Sometimes you don't always want to execute an action even though it has already
been added to the queue. You can add one or more constraints to prevent it from
executing. For example, you might use a constraint "Updated Recently" with the
"Set Moderation State" action to prevent it from being archived if someone has
updated it within the last 30 days.

Important: Constraints are only evaluated during _execution_. They do not
restrict what can be added to the queue. That should be done with your own
validation.

### Action Queue Item Entity

All queued actions are stored as entities. When the trigger is invoked it will
find all the action queue entities and execute the action plugins associated
with them.

Because action queue items are entities you can use them with views and other
features that support entities.

HOW TO USE THIS MODULE
------------
To get started follow these steps:
1. Create a new Action plugin.
2. Create a new ActionQueueTrigger plugin.
3. Create a new ActionQueueConstraint plugin (optional).
4. Call `ActionWaitQueue::addItem($action, $entity)` to add your action to the
   queue.
6. Call `ActionWaitQueue::triggerActions($trigger, $entiies);`

### Create a new Action plugin

Create a new plugin that extends `ActionQueueActionBase` and has the `trigger` annotation.

```php
/**
 * Example of an action queue action plugin implementation.
 *
 * @Action(
 *   id = "archive_content",
 *   label = @Translation("Archive content on date."),
 *   category = @Translation("example"),
 *   type = "node",
*    action_queue = {
*      "trigger" = "on_date",
*    }
 * )
 */
class ArchiveOnDate extends ActionQueueActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL) {
    // Your content is archived here.
  }
}
```


### Create an ActionQueueTrigger plugin
This plugin is responsible for evaluating the trigger. If the trigger evaluation
passes then any actions tagged with that trigger will fire.

```php
/**
 * Trigger action on date.
 *
 * @ActionQueueTrigger(
 *   id = "on_date",
 *   admin_label = @Translation("On date"),
 * )
 */
class OnDate extends ActionQueueTriggerBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'date' => strtotime('+180 days'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate($entity, $arguments = []) {
    $current_time = \Drupal::time()->getCurrentTime();
    $arg_time = $arguments['date'];
    if ($current_time >= $arg_time) {
      return TRUE;
    }
    return FALSE;
  }
}
```

### Create an ActionQueueConstraint Plugin (Optional)
This plugin is responsible for evaluating any additional conditions before the
action executes. If the constraint fails then the action won't fire.

```php
/**
 * Updated recently constraint.
 *
 * @ActionQueueConstraint(
 *   id = "updated_recently",
 *   label = @Translation("If content was updated recently"),
 * )
 */
class UpdatedRecently extends ActionQueueConstraintBase {

  /**
   * {@inheritdoc}
   */
  public function validate() {
    // The constraint fails if the content was updated within the last 30 days.
    if ($this->getEntity()->getChangedTime() > time() - strtotime('-30 days')) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->t('This content was updated recently and cannot be archived.');
  }

}
```

Add the constraints to your action plugin.

```php
/**
 * @Action(
 *   id = "archive_content",
 *   label = @Translation("Archive content on date."),
 *   category = @Translation("example"),
 *   type = "node",
 *   action_queue = {
 *     "trigger" = "on_date",
 *     "constraints" = {
 *       "updated_recently" = {},
 *     },
 *   }
 * )
 */
```

### Call ActionWaitQueue::addItem($action, $entity);
As part of the feature you are working on use the wait queue to enqueue the
action. For example, perhaps you're doing this as part of a form submission.

```php
public function submitForm(array $form, FormStateInterace $form_state) {

  $entity = $this->entityTypeManager->getStorage('node')->load($form_state->getValue('nid'));

  // Get the action wait queue service.
  $actionWaitQueue = \Drupal::service('action_queue.wait_queue');

  // This creates a new action plugin.
  $action = $actionWaitQueue->prepareAction('archive_content', [], ['date' => '2022-01-01']);

  // This adds the action to the queue.
  $actionWaitQueue->addItem($action, $entity);
}

```
### Call ActionWaitQueue::triggerActions();
Queued actions do not execute automatically. You will need to execute them
yourself by calling `triggerActions()`. You can also think of this like calling
the event dispatcher or invoking a hook.

```php
function example_module_cron() {
  $actionWaitQueue = \Drupal::service('action_queue.wait_queue');
  $waitQueue->triggerActions('on_date');
}
```
